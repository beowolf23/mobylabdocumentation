---
title: Publicarea unei imagini într-un registru
sidebar_position: 7
---

Mai devreme, am creat o imagine de Docker pe care am rulat-o local într-un container. Pentru a putea rula imaginea creată în orice alt sistem, este necesar să o publicăm, deci să o urcăm într-un registru pentru a putea să facem deploy de containere cu imaginea noastră în producție. Un registru este o colecție de repository-uri, iar un repository este o colecție de imagini (similar cu GitHub, cu diferența că, într-un registru Docker, codul este deja construit și se rețin modificările făcute în straturile imaginilor de Docker, nu în cod). Există numeroase registre pentru imagini Docker (Docker Hub, Gitlab Registry, etc.), iar la laborator vom folosi registrul public Docker, pentru că este gratuit și pre-configurat.

Pentru exemplificare, vom porni de la aplicația prezentată anterior, care afișează o poză aleatoare într-o pagină Web. Primul pas în publicarea unei imagini este crearea unui cont la https://hub.docker.com. Mai departe, logarea în registru de pe mașina locală se realizează prin următoarea comandă:
```shell
$ docker login
 
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: 
Password: 
Login Succeeded
```

Putem specifica numele de utilizator și parola direct în comandă, iar varianta generică a acesteia este (unde serverul implicit, dacă alegem să omitem acel parametru, este Docker Hub):
```shell
$ docker login [–u <UTILIZATOR> –p <PAROLĂ>] [SERVER]
```

Înainte de a publica imaginea în registru, ea trebuie tag-uită după formatul ***username/repository:tag***. Tag-ul este opțional, dar este util pentru că denotă versiunea unei imagini Docker. Se folosește următoarea comandă pentru tag-uirea unei imagini (în exemplul de mai jos, unde vrem să tag-uim imaginea pe care am creat-o anterior, utilizatorul se numește ***raduioanciobanu***, repository-ul este ***cloudcomputing***, iar tag-ul este example):
```shell
$ docker tag testapp raduioanciobanu/cloudcomputing:example
```

```shell
$ docker images
REPOSITORY                       TAG                 IMAGE ID            CREATED              SIZE
testapp                          latest              74254b15e6ba        About a minute ago   62.9MB
raduioanciobanu/cloudcomputing   example             74254b15e6ba        About a minute ago   62.9MB
alpine                           edge                f96c4363411f        4 weeks ago          5.58MB
```

Odată tag-uită imaginea, ea poate fi publicată în registru:
```shell
$ docker push raduioanciobanu/cloudcomputing:example
```

Din acest moment, imaginea va fi vizibilă pe https://hub.docker.com, de unde poate fi descărcată și rulată pe orice mașină, server sau sistem Cloud:
```shell
$ docker run -p 8888:5000 raduioanciobanu/cloudcomputing:example
 
Unable to find image 'raduioanciobanu/cloudcomputing:example' locally
example: Pulling from raduioanciobanu/cloudcomputing
cc5efb633992: Pull complete 
cd0af7ebab8a: Pull complete 
41c55a3da379: Pull complete 
a779b27637f8: Pull complete 
dfaeccf28d0c: Pull complete 
805843c75452: Pull complete 
Digest: sha256:25af18fb4ffa9bf439e90bd4baee9adf0ab1e2999a44aeaa216ebf0454201ce8
Status: Downloaded newer image for raduioanciobanu/cloudcomputing:example
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
[...]
```

Alternativ, în loc să rulăm comanda de publicare a unei imagini de fiecare dată când modificăm ceva la codul sursă, putem să configurăm [build-uri automate](https://docs.docker.com/docker-hub/builds/) din contul de Docker Hub. Pașii necesari sunt descriși în continuare. În primul rând, este necesară existența unui repository Docker Hub și a unui repository pe GitHub (Docker Hub funcționează și cu BitBucket, dar în acest exemplu ne vom concentra pe GitHub). Toate fișierele necesare creării unei imagini Docker (adică Dockerfile-ul și toate fișierele sursă și de configurare) trebuie să fie prezente în repository-ul GitHub. Mai departe, de pe pagina repository-ului de Docker Hub, se selectează tab-ul Builds și apoi opțiunea "Configure Automated Builds", așa cum se poate observa în imaginea de mai jos.

<img alt="img" src="/img/cloud-computing/dockerhub.png" width="50%" style={{margin: "auto", display: "block"}} />

În continuare, va fi necesară completarea unor informații despre repository-ul GitHub și opțiuni de testare automată înainte de build, după care trebuie specificate regulile de build. O regulă de build conține informații despre: tipul build-ului (bazat pe un branch sau pe un tag Git), sursa (numele branch-ului sau tag-ului de pe care se face build-ul), tag-ul care va fi asignat noii imagini Docker construite, numele și adresa fișierului Dockerfile în repository-ul GitHub, calea către sursele ce vor fi compilate, opțiuni de auto-build (dacă se va face build automat la fiecare push pe branch-ul sau cu tag-ul specificat), opțiuni de build caching (dacă se vor cache-ui fișiere la build în cazul unor repository-uri de dimensiuni mari). În exemplul de mai jos, atunci când are loc un push pe branch-ul ***master***, se va crea automat o imagine Docker cu tag-ul ***latest*** folosindu-se fișierul Dockerfile aflat în rădăcina repository-ului de GitHub.

<img alt="img" src="/img/cloud-computing/dockerhub2.png" width="50%" style={{margin: "auto", display: "block"}} />

În continuare, pe pagina de Builds de pe Docker Hub vor exista opțiuni pentru pornirea unui nou build, precum și informații despre build-urile precedente și statusurile lor.

