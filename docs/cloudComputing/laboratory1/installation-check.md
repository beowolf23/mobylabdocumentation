---
title: Testarea instalării
sidebar_position: 5
---

Pentru a verifica dacă instalarea s-a realizat cu succes, putem rula un container simplu de tip Hello World:
```shell
$ docker container run hello-world
 
Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
5b0f327be733: Pull complete
Digest: sha256:b2ba691d8aac9e5ac3644c0788e3d3823f9e97f757f01d2ddc6eb5458df9d801
Status: Downloaded newer image for hello-world:latest
 
Hello from Docker!
This message shows that your installation appears to be working correctly.
 
To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.
 
To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash
 
Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/
 
For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

Outputul execuției ne arată pașii pe care Docker îi face în spate pentru a rula acest container. Mai precis, dacă imaginea pe care dorim să o rulăm într-un container nu este disponibilă local, ea este descărcată din repository, după care se creează un nou container pe baza acelei imagini, în care se rulează aplicația dorită.