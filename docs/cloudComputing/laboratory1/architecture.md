---
title: Arhitectura Docker
sidebar_position: 3
---

Docker are o arhitectură de tip client-server, așa cum se poate observa în imaginea de mai jos (preluată din [documentația oficială Docker](https://docs.docker.com/get-started/)). Clientul Docker comunică, prin intermediul unui API REST (peste sockeți UNIX sau peste o interfață de rețea), cu daemon-ul de Docker (serverul), care se ocupă de crearea, rularea și distribuția de containere Docker. Clientul și daemon-ul pot rula pe același sistem sau pe sisteme diferite. Un registru Docker are rolul de a stoca imagini.

<img alt="img" src="/img/cloud-computing/architecture-docker.png" width="50%" style={{margin: "auto", display: "block"}} />
