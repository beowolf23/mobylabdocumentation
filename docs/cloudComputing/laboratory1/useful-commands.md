---
title: Comenzi utile
sidebar_position: 11
---

:::tip
Vă recomandăm să vă documentați de pe [site-ul oficial Docker](https://docs.docker.com/reference/), întrucât comenzile oferite aici sunt minimul necesar de care aveți nevoie să puteți lucra cu Docker. În realitate, sunt mult mai multe comenzi, fiecare din ele având mult mai multe argumente.
:::

## Sistem
```shell
$ docker <COMANDĂ> --help  # afișează informații complete despre o comandă
$ docker version           # afișează versiunea și detalii minore despre Docker
$ docker info              # afișează informații complete despre Docker
$ docker system prune      # eliberează spațiu utilizat inutil
```

## Interacțiune imagini
```shell
$ docker image pull <IMAGINE>     # descarcă imaginea în cache-ul local
$ docker build -t <TAG> .         # construiește o imagine plecând de la Dockerfile care se află în folderul curent
 
$ docker image ls                 # afișează imaginile descărcate
$ docker images                   # afișează imaginile descărcate
 
$ docker image rm <IMAGINE>       # șterge o imagine din cache-ul local
$ docker rmi <IMAGINE>            # șterge o imagine din cache-ul local
 
$ docker image inspect <IMAGINE>  # afișează informații despre o imagine
```

## Interacțiune containere
```shell
$ docker container run <IMAGINE> [COMANDĂ]  # rulează un container, și opțional îi trimite o comandă de start
$ docker container run -it <IMAGINE>        # rulează un container în mod interactiv
$ docker container run -d <IMAGINE>         # rulează un container în background (ca daemon)
 
$ docker exec -it <IMAGINE> <COMANDĂ>       # pornește un terminal într-un container deja pornit și execută o comanda
 
$ docker container ls                       # arată o listă cu toate containerele care rulează
$ docker container ls -a                    # arată o listă cu toate containerele rulate pe sistem
$ docker container inspect <ID>             # afișează informații despre un container
 
$ docker attach <ID>                        # se atașează la un container
$ docker stop <ID>                          # oprește un container
$ docker restart <ID>                       # repornește un container
$ docker rm <ID>                            # șterge un container
 
$ docker ps                                 # afișează containerele care rulează
$ docker logs <ID>                          # afișează loguri din interiorul unui container
$ docker top <ID>                           # afișează procesele care rulează într-un container
```

:::tip
Diferența dintre comenzile ***exec*** și ***attach*** (care pot părea similare) este că, la ***attach***, se asociază un terminal la container, ceea ce înseamnă că, dacă se iese din acel terminal, se iese cu totul și din container.
:::

## Lucru cu registre
```shell
$ docker login [–u <UTILIZATOR> –p <PAROLĂ>] [SERVER]   # loghează un utilizator într-un registru
$ docker tag <IMAGINE> <UTILIZATOR/REPOSITORY:TAG>      # dă un tag unei imagini pentru upload în registru
$ docker push <UTILIZATOR/REPOSITORY:TAG>               # uploadează o imagine în registru
```

## Creare și interacțiune cu rețele
```shell
$ docker network create -d <DRIVER> <REȚEA>          # creează o rețea cu un driver dat
$ docker network ls                                  # afișează rețelele existente
$ docker network rm                                  # șterge o rețea
$ docker network connect <REȚEA> <CONTAINER>         # conectează un container la o rețea
$ docker network disconnect <REȚEA> <CONTAINER>      # deconectează un container de la o rețea
$ docker network inspect <REȚEA>                     # afișează informații despre o rețea
$ docker container run --network=<REȚEA> <IMAGINE>   # pornește un container într-o rețea
```

## Creare și interacțiune cu volume sau bind mounts
```shell
$ docker volume create <VOLUM>                                                # creează un volum
$ docker volume ls                                                            # afișează volumele existente
$ docker volume rm <VOLUM>                                                    # șterge un volum
$ docker container run -v <VOLUM> <IMAGINE>                                   # rulează un container cu un volum atașat
$ docker container run -v <SURSĂ>:<DESTINAȚIE> <IMAGINE>                      # rulează un container cu un volum sau un bind mount atașat
$ docker container run --mount source=<SURSĂ>,target=<DESTINAȚIE> <IMAGINE>   # rulează un container cu un volum sau bind mount atașat
```