---
title: Deployments & virtual machines în AWS
sidebar_position: 2
---

În cadrul acestui laborator veți învăța cum să creați mașini virtuale în cadru AWS (Amazon Web Services), care reprezintă o platformă care furnizează servicii de tip cloud computing, baze de date, stocare de fișiere, device farms, etc.

Pentru acest laborator presupunem că avem deja Terraform instalat de la laboratorul precedent. Dacă nu aveți Terraform instalat, puteți urma pașii din laboratorul anterior.

Pe lângă Terraform, avem nevoie de AWS CLI instalat. Pentru a instala, urmați pașii de [aici](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

Pe lângă asta, avem nevoie de un cont pentru Amazon AWS. După ce ați făcut pașii pentru a avea propriul cont AWS, vrem sa conectăm AWS CLI la consola de AWS.

Pentru a face asta, mergem în AWS și intrăm în secțiunea IAM:
<img alt="img" src="/img/cloud-computing/lab-5-image3.png" width="75%" style={{margin: "auto", display: "block"}} />

Apoi mergem la "My security credentials":
<img alt="img" src="/img/cloud-computing/lab-5-image1.png" width="75%" style={{margin: "auto", display: "block"}} />

Următorul pas este să mergem la "Access Keys" și să apăsăm pe butonul de `Create New Access Key`:
<img alt="img" src="/img/cloud-computing/lab-5-image5.png" width="75%" style={{margin: "auto", display: "block"}} />

Păstrați fișierul generat. Rulați comanda `aws configure` și introduceți datele generate în fisierul creat anterior (`AWSAccessKeyId` și `AWSSecretKey`).

Pentru a crea o instanță simplă de EC2 (o mașina virtuală) folosim următorul fișier de Terraform:
```terraform
terraform {
 required_providers {
   aws = {
     source  = "hashicorp/aws"
     version = "~> 4.16"
   }
 }
 
 required_version = ">= 1.2.0"
}
 
provider "aws" {
 region = "us-west-2" # regiunea in care o sa se faca deploy la resurse
}
 
resource "aws_instance" "app_server" {
 ami           = "ami-830c94e3" # tipul de masina folosita (ami = Amazon Machine Image)
 instance_type = "t2.micro" # tipul de instanta (resursele pe care le are masina)
 
 tags = {
   Name = "labAwsTerraform"
 }
}
```

Rulați comenzile cunoscute pentru a face deployment acestei infrastructuri:
```shell
terraform init
terraform fmt
terraform plan
terraform apply
```

Pentru a verifica ca mașina virtuală a fost deployed, căutăm serviciul EC2:
<img alt="img" src="/img/cloud-computing/lab-5-image7.png" width="75%" style={{margin: "auto", display: "block"}} />

Ajungem în pagina următoare în care vedem că nu avem nicio instanță up and running, deși planul nostru de Terraform a fost executat cu succes. De ce?
<img alt="img" src="/img/cloud-computing/lab-5-image4.png" width="75%" style={{margin: "auto", display: "block"}} />

Observați diferența de regiuni (Frankfurt, în cazul din imaginea anterioară, față de us-west-2 în Terraform). Pentru a modifica regiunea în care facem deployment la resurse modificăm codul Terraform. În cazul de față, putem să facem click pe Frankfurt și să selectăm us-west-2 (Oregon). În acest moment putem vedea că avem o instanță up and running:
<img alt="img" src="/img/cloud-computing/lab-5-image2.png" width="75%" style={{margin: "auto", display: "block"}} />

:::tip
În codul Terraform modificați numele masinii din `labAwsTerraform` în `testUpdate`. Rulatț comenzile obișnuite pentru a vedea planul. Observați că această modificare nu implică distrugerea mașinii existente, ci doar modificarea in-place.
:::

<img alt="img" src="/img/cloud-computing/lab-5-image6.png" width="75%" style={{margin: "auto", display: "block"}} />