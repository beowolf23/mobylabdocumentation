---
title: Instalare Terraform
sidebar_position: 3
---

Folosim site-ul oficial al Terraform pentru a instala tool-ul. Găsiți toate detaliile necesare aici: https://www.terraform.io/downloads

Pentru a instala Terraform pe o mașină cu un sistem de operare Ubuntu / Debian, folosim următoarele comenzi:
```shell
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install terraform
```

Pentru a verifica dacă instalarea fost efectuată cu succes, încercăm să obținem versiunea de Terraform instalată: `terraform -version`

:::tip Terraform autocomplete
Pentru a putea utiliza funcția de autocomplete cu Terraform: `terraform -install-autocomplete`
:::