---
title: Deployments
sidebar_position: 10
---

Un deployment ne dă opțiunea declarativă de a updata pod-uri și ReplicaSets. Într-un deployment descriem starea dorită, apoi un Deployment Controller are grijă ca clusterul să ajungă în starea descrisă. Putem folosi deployment-uri pentru a crea noi ReplicaSets sau chiar pentru a șterge un deployment existent și a adopta toate resursele sale.

Exemplu de deployment (varianta declarativă):
```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
 name: nginx-deployment
 labels:
   app: nginx
spec:
 replicas: 3
 selector:
   matchLabels:
     app: nginx
 template:
   metadata:
     labels:
       app: nginx
   spec:
     containers:
     - name: nginx
       image: nginx:1.14.2
       ports:
       - containerPort: 80
```

:::note Task
- Creați deployment-ul definit în fișierul de mai sus, știm deja cum să folosim comanda `apply`.
- Verificați câte noduri există: `kubectl get pods`.
- Verificați câte ReplicaSets există: `kubectl get rs # am folosit un shortcut aici`
- Verificați că deployment-ul este up and running: `kubectl get deploy`
:::