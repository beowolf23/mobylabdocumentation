---
title: Generarea de fișiere YAML
sidebar_position: 7
---

Fișierele YAML pot fi scrise de la 0 sau pot fi generate prin rularea *uscată* a pod-urilor.

```yaml
#Exemplu oficial de fisier YAML
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    run: nginx
    whatever: dude
spec:
 containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
```

Pentru a genera fișiere YAML plecând de la o comandă imperativă putem folosi flag-urile `–dry-run=client -o yaml`. Exemplu: `kubectl run nginx --image=nginx --dry-run=client -o yaml`
