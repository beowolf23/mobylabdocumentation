---
title: Rollouts
sidebar_position: 3
---

Am realizat că avem o greșeală în deploymentul nostru, pentru a reveni la versiunea anterioară folosim următoarea comandă: `kubectl rollout history deployment <depname>`

:::note Tasks
- Analizați din nou obiectele de ReplicaSet și Pod.
- Scalați deployment-ul la 2 replici: `kubectl scale deployment <depname> –replicas=2`
- Verificați numărul de pod-uri.
- Scalați din nou la 5 replici: `kubectl scale deployment <depname> –replicas=5`
:::

Pentru a modifica imaginea folosită de un deployment, avem următoarea comandă: `kubectl set image deployment <depname> nginx=nginx:failTest` În comanda de mai sus am introdus intenționat o imagine care nu există. Dacă urmariți comportamentul deployment-ului, o să observați că noile pod-uri nu vor fi create deoarece nu poate fi gasită imaginea specificată.

Am ajuns în situația în care vrem să ne întoarcem la o versiunea anterioară. Pentru a vedea istoricul modificărilor făcute pe acest deployment, avem următoarea comandă: `kubectl rollout history deployment <depname>`

Observați ca avem mai multe "revizii" ale deployment-ului nostru. Pentru a ne întoarce la o anumită revizie, folosim următoarea comandă: `kubectl rollout history deployment cc-dep-01 –revision=2`