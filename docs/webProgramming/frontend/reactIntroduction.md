---
title: Introducere în NodeJs și React.js
sidebar_position: 2
---

## Ce este React.js?

React.js este o bibliotecă JavaScript utilizată pentru crearea interfețelor utilizator web dinamice și interactive. A fost dezvoltată de Facebook și este folosită pentru construirea de aplicații web moderne. React.js este construită în jurul unui concept numit “componente”, care reprezintă bucăți de cod reutilizabile și independent gestionate, ceea ce face ca dezvoltarea aplicațiilor web să fie mai ușoară și mai eficientă.

<img src="/img/web-programming/react.png" width="300" style={{float: "right"}} />

## De ce să folosiți React.js?

Există mai multe motive pentru a folosi React.js în dezvoltarea aplicațiilor web. Iată câteva dintre cele mai importante:

- Reutilizarea codului: React.js încurajează dezvoltatorii să creeze componente reutilizabile, ceea ce poate reduce semnificativ timpul de dezvoltare și poate duce la cod mai curat și mai ușor de întreținut.
- Performanța: React.js are o performanță excelentă datorită faptului că utilizează Virtual DOM, o tehnică care optimizează modul în care sunt actualizate paginile web, fără a necesita o reîncărcare completă a paginii.
- Comunitatea mare: React.js are o comunitate mare și activă de dezvoltatori, care împărtășesc cunoștințe și dezvoltă instrumente și biblioteci suplimentare care pot fi utilizate împreună cu React.js.

## Ce face React.js diferit față de alte framework-uri?

React.js diferă față de alte framework-uri prin abordarea sa modulară și component-based. În timp ce alte framework-uri web se concentrează pe construirea de pagini web întregi, React.js se concentrează pe construirea de componente reutilizabile. Această abordare modulară face ca dezvoltarea să fie mai ușoară și mai eficientă, deoarece dezvoltatorii pot construi și testa componente independent, apoi le pot folosi în diferite aplicații web fără a fi nevoie să le rescrie. De asemenea, React.js utilizează Virtual DOM pentru a gestiona actualizările paginii, ceea ce face ca aplicațiile să fie mai rapide și mai eficiente decât cele construite folosind alte framework-uri.

## NodeJs

<img src="/img/web-programming/nodejs.png" width="300" style={{float: "left"}} />

Ca să lucrați cu React.js mai întâi este nevoie să instalați **NodeJs** și să creați un proiect. NodeJs este un runtime de JavaScript/TypeScript inițial gândit să fie folosit pentru rularea de cod JavaScript pe server în ideea că dezvoltatorii web să poata lucra doar cu un singur limbaj iar pentru proiecte mici spre medii sau pentru prototipare este un mediu adecvat de dezvoltare a unui produs. NodeJs este construit cu același motor pentru Javascript, **V8**, pe care îl folosesc și browser-ele bazate pe **Chromium**. 

Cu toate acestea, NodeJs este folosit și pentru aplicații web de frontend **SPA (Single-Page Application)**. Aici NodeJs este folosit pentru a compila aplicații frontend cu ajutorul unor biblioteci sau framework-uri cum sunt React.js, Angular sau Vue.js care vor fi impachetate într-un fisier JavaScript minificat (se minimizează dimensiunea fisierului script) ca să fie servit de catre un server. Aplicația va fi cerută de catre un browser, va fi încărcat fișierul cu aplicația iar browser-ul va randa dinamic pagina web în funcșie de codul JavaScript. Trebuie reținut ca NodeJs pentru aceste aplicații este folosit doar la dezvoltare si compilare.

NodeJs vine la pachet cu câteva utilitare, **npx** și **npm**. Acestea sunt folosite pentru a lucra cu diferitele biblioteci de JavaScript/TypeScript pe care le puteți găsi și pe [npmjs](https://www.npmjs.com/).

Vom folosi mai întâi utilitarul **npx** pentru a crea primul proiect de React, acest utilitar este folosit pentru a rula diferite comenzi din pachete de **NPM (Node Pachage Manager)** ca în felul următor:

```sh
npx create-react-app my-app --template typescript
```

:::tip
Există și alte șabloane de proiect cu diferite unelte de build pentru proiectele de React cu diferite avantaje cum ar fi:
```sh showLineNumbers
npm create vite@latest my-app -- --template react-ts # pentru Vite
npx create-next-app@latest # pentru Next.js
```
:::

Aceasta comandă va crea un proiect minimal de React deja initializat cu un fisier **package.json** și cateva componente. Acest fisier **JSON** este un fișier de configurare pentru proiect unde se declară pachetele NPM folosite și scripturile pentru comenzi de executat pentru NodeJs. Ca să instalași noi pachete puteți da direct comanda urmatoare în directorul cu acest fisier JSON:

```sh
npm install jwt-decode
```

Sau să adaugati pachetul în **package.json** cu versiunea dorita în câmpul `dependencies` și dând comanda:

```sh
npm install
```

Comanda de instalare va initializa sau actualiza fisierul **package-lock.json**, acesta este necesar să existe. Este folosit ca persoanele care lucreaza la același proiect să lucreze cu exact aceleași versiune de biblioteti pentru ca pot fi instalate în loc de versiunile exacte de pachete versiuni compatibile și sa nu existe discrepanțe între mediile de dezvoltare când se lucrează cu proiectul.

În **package.json** se afla și scripturi pentru a fi rulate de NodeJs în câmpul `scripts`:

```json showLineNumbers
{
    "scripts": 
    {
        "start": "node ./index.js"
    }
}
```

Acest script de start poate fi invocat astfel:

```sh
npm run start
```

:::tip
Ca să vă ajutăm pentru proiect aveți [aici](https://gitlab.com/mobylabwebprogramming/reactfrontend) un proiect de bază de la care să plecați ca să vă implementati frontendul pentru proiect.
:::

## Componente React

Veți observa că într-o aplicație React componentele vizuale sunt declarate în fișiere **JSX/TSX (JavaScript/TypeScript XML)**. Practic, în aceste fisiere se intercalează scriptul cu componente **HTML** (este impropriu spus dar se pot trata ca fiind tag-uri HTML normale). Acest lucru ajută dezvoltatorii să își definească componentele vizuale la fel de ușor ca în cod HTML normal dar declarând logica componentei mai ușor direct în script.

În React, componentele sunt blocuri de construcție fundamentale ale interfeței utilizatorului. Acestea sunt reutilizabile și pot fi combinate pentru a crea interfețe complexe. Există două tipuri de componente în React:

- Componente bazate pe clase: Acestea sunt componente care sunt scrise sub formă de clase și extind clasa React.Component. Aceste componente au acces la **proprietățile (props)** și **starea (state)** componentei, precum și la diferite metode ale ciclului de viață (lifecycle) al React.

```tsx showLineNumbers
class HelloWorld extends React.Component {
  render() {
    return <h1>Hello, World!</h1>;
  }
}
```

- Componente funcționale: Acestea sunt componente care sunt scrise sub formă de funcții și returnează o componentă TSX care descrie cum ar trebui să arate componenta.

```tsx showLineNumbers
function HelloWorld() {
  return <h1>Hello, World!</h1>;
}

const HelloWorld = () => {
  return <h1>Hello, World!</h1>;
}
```
:::tip
Recomandăm sa fie folosite componente funcționale in loc de varianta bazata pe clase pentru ca se face uz foarte mult de functii speciale numite "hooks" iar din acest motiv se tinde spre o abordare functionala pentru codul din React.
:::

## Proprietăți (props) și stare (state) în React.js

Proprietățile (props) sunt argumente pe care le puteți transmite în componente. Acestea sunt folosite pentru a personaliza comportamentul unei componente și pot fi accesate prin intermediul obiectului props.

Exemplu de utilizare a proprietăților într-o componentă funcțională:

```tsx showLineNumbers
function Welcome(props) {
  return <h1>Hello, {props.name}!</h1>;
}
const element = <Welcome name="John" />;
ReactDOM.render(element, document.getElementById('root'));
```

:::note
Aici, `name` este o proprietate transmisă către componenta `Welcome`. Această proprietate poate fi accesată în componentă prin intermediul obiectului props.
:::

Starea (state) este o altă caracteristică importantă a componentelor React. Starea este utilizată pentru a gestiona datele componentelor care se pot schimba în timp. Starea este definită prin funcția hook **useState** care returnează pentru o stare inișială obiectul cu starea și o funcție de modificare a stării.

Exemplu de utilizare a stării într-o componentă bazată pe clasă:

```tsx showLineNumbers
import React, { useState } from 'react';

const Counter = () => {
  const [state, setState] = useState({ count: 0 })

  return <div>
    <p>Count: {state.count}</p>
    <button onClick={() => setState({ count: state.count + 1 })}>
      Increment
    </button>
  </div>
}

ReactDOM.render(<Counter />, document.getElementById('root'));
```

:::note
În acest exemplu, starea este definită prin **useState** și reprezintă numărul de apăsări ale butonului. Butonul declanșează metoda setState, care actualizează starea și determină re-renderizarea componentei. Orice modificare asupra obiectului stării direct nu atrage aspura sa re-randarea componentei.
:::

## Ce sunt functiile hook?

Funcșiile hook sunt funcșii speciale ce se apeleaza rând pe rând în aceiași ordine atunci când o componenta se re-randează. Cu ajutorul acestor funcții se poate implementa foarte ușor logică complexă pentru UI independent de componenta vizuala compunând asemenea funcții mai complexe din funcții hook mai simple. Aceste funcții trebuie numite cu `use` în față la fel ca `useState` și pot primi date ca orice funcție normală și returna date care pot proveni din apelul altor funcții hook.

:::danger
Funcțiile hook trebuie apelate în aceiași ordine mereu și nu pot fi apelate în mod condiționat, de exemplu, apelate în interiorul unui bloc dintr-un `if`, dar pot să contină blocuri de cod apelate condiționat.
:::

## Referinte
- [GitLab Schelet Frontend](https://gitlab.com/mobylabwebprogramming/reactfrontend)
- [Node.js](https://nodejs.org/en/download/)
- [VSCode](https://code.visualstudio.com/download)
- [WebStorm](https://www.jetbrains.com/webstorm/download/)
- [Tutorial React.js](https://react.dev/learn/tutorial-tic-tac-toe)
