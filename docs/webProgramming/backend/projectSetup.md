---
title: Pregătirea unui proiect
sidebar_position: 2
---

Pentru a vă ajuta în implementarea proiectului de la laborator, puteți utiliza scheletul proiectului .NET disponibil pe [GitLab](https://gitlab.com/mobylabwebprogramming/dotnetbackend). Cu toate acestea, pentru a începe, vom crea un proiect de backend în IDE-ul preferat, selectând un template pentru .NET Core de Web API, după cum se arată mai jos.

## Visual studio

<img alt="img" src="/img/web-programming/firstProjectSelectVisualStudio.png" />
<img alt="img" src="/img/web-programming/projectNamingVisualStudio.png" />
<img alt="img" src="/img/web-programming/projectConfigurationVisualStudio.png" />

## Rider
<img alt="img" src="/img/web-programming/firstProjectRider.png" />

Observați că punctul de intrare în program este fișierul Program.cs, daca nu s-a selectat "Do not use top-level statements" aici nu va aparea o clasa cu o funcție main statică și dar va exista imiplicit. Aici se creează un obiect care reprezintă aplicația web și poate fi configurat, cum este exemplificat în configurarea pentru autorizare, maparea controllerelor sau configurarea Swagger. Aceste aspecte vor fi detaliate mai mult în laboratoarele viitoare.

<img alt="img" src="/img/web-programming/projectViewMainVisualStudio.png" />

În limbajul C#, un proiect este creat în cadrul unei soluții. Solutia gestionează mai multe proiecte și poate fi utilizată pentru a menține referințe între ele. De exemplu, puteți reutiliza clase comune între diferite proiecte de backend prin intermediul unei biblioteci partajate referențiate de amândouă.

Veți găsi și un fișier appsettings.json, care conține un JSON pentru configurarea aplicației. Acesta va fi utilizat pentru a seta variabile folosite de backend. În plus, veți găsi un controller foarte simplu care va răspunde la cererile HTTP de la clienți. Este important să rețineți că în dezvoltarea de aplicații web se folosește frecvent Dependency Injection. Deși acest concept va fi detaliat mai mult în cele ce urmează, puteți observa în cod că componentele sau clasele din proiect, cum ar fi WeatherForecastController, nu sunt instantiate în mod explicit. Instantierea se face implicit de către framework, calculând dependențele componentei și injectându-le în componentă la instanțiere prin constructor, cum se întâmplă și cu logger-ul din exemplul dat.

Rulați aplicația de backend, iar în browser va fi deschisă o pagină cu Swagger, sau cu denumirea sa alternativă, OpenAPI Specification. Swagger-ul este o interfață simplă pentru testarea cererilor HTTP fără a fi nevoie de un client HTTP. De asemenea, descrie întregul API HTTP al serverului, inclusiv rutele și tipurile de date schimbate cu clientul. Încercați să executați o cerere către backend direct din pagina Swagger-ului. Această facilitate este importantă deoarece ușurează testarea backend-ului și descrie modul în care API-ul poate fi folosit, permițând generarea automată a clienților HTTP pentru aplicațiile care consumă API-ul expus.

<img alt="img" src="/img/web-programming/swagger.png" />

Pentru a crea un backend, este important să acumulați cunoștințele necesare. Acestea vor fi expuse treptat în următoarele secțiuni.

## Sarcini de laborator

Instalați uneltele necesare pentru laborator și cereți ajutor dacă întâmpinați dificultăți.

Urmați codul din `WeatherForecastController` și încercați să-l modificați mergând pe intuiție și testând cu Swagger. Dacă aveți întrebări, adresați-le asistentului/asistentei. Mai multe informații vor fi furnizate în laboratoarele următoare.

Descărcați template-ul de proiect de pe [GitLab-ul grupului nostru](https://gitlab.com/mobylabwebprogramming/dotnetbackend) și încercați să parcurgeți codul, folosind explicațiile din comentarii. Dacă nu este necesar pentru acest laborator, dar puteți să-l folosiți ca punct de plecare pentru proiectul de la laborator.

:::tip
Dacă sunteți interesați de capacitățile .NET, accesați [NuGet](https://www.nuget.org/) și explorați pachetele pentru C#. NuGet este un manager de pachete care oferă o gamă largă de biblioteci ce pot fi instalate în proiecte. Pentru Visual Studio, accesați `Tools > NuGet Package Manager > Manage NuGet Package for Solution...`, iar pentru Rider, accesați `Tools > NuGet > Manage NuGet Package for Solution`.
:::
