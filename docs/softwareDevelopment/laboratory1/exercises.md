---
title: Exerciții
sidebar_position: 8
---

## Comenzi de bază

1. Aduceți în cache-ul local imaginea <em>**busybox**</em> din registrul oficial Docker.
2. Rulați un container de <em>**busybox**</em> care să execute comanda <em>**uptime**</em>.
3. Rulați un container interactiv de <em>**busybox**</em>. Odată ce ați intrat în el, executați
comanda <em>**wget google.com**</em>, apoi ieșiți.
4. Rulați un container interactiv detașat (daemon) de <em>**busybox**</em>. Odată ce l-ați
pornit, atașați-vă la el și dați comanda <em>**id**</em>, apoi ieșiți.
5. Ștergeți toate containerele și imaginile create la punctele precedente.

## Crearea unei imagini

:::tip
Pentru exercițiile următoare, veți porni de la
[această arhivă](/files/softwareDevelopment/homework1.zip), care
conține o aplicație simplă NodeJS.
:::

1. Pornind de la cele două fișiere din arhivă, scrieți un Dockerfile care va crea o imagine urmărind pașii de mai jos:
   - se va porni de la cea mai recentă versiune a imaginii oficiale de NodeJS,
   adică <em>**node:21-alpine3.18**</em>
   - se va copia fișierul <em>**package.json**</em> din arhivă în directorul curent (./); acest fișier
   are rolul de a specifica dependențele aplicației NodeJS (de exemplu, framework-ul Express.js)
   - se va rula comanda <em>**npm install**</em> pentru a instala dependețele din fișierul de la
   pasul precedent
   - se va copia sursa <em>**server.js**</em> în directorul de lucru <em>**/usr/src/app/**</em>
   - se va expune portul 8080
   - în final, se va menționa comanda de rulare a aplicației; astfel, se va rula fișierul 
   <em>**/usr/src/app/server.js**</em> cu binarul <em>**node**</em>.
2. Folosiți Dockerfile-ul scris anterior pentru a crea o imagine numita <em>**nodejstest**</em>.
3. Porniți un container care să ruleze imaginea <em>**nodejstest**</em> pe portul 12345 în modul
detașat (daemon). Verificați că funcționează corect intrând pe
[http://127.0.0.1:12345](http://127.0.0.1:12345) (ar trebui să vă apară un mesaj de Hello World).

## Lucrul cu rețele, volume și bind mounts

:::tip
Pentru exercițiile următoare, veți porni de la
[această arhivă](/files/softwareDevelopment/homework2.zip), care
conține o aplicație NodeJS care realizează un API de adăugare de cărți într-o bibliotecă peste
o bază de date PostgreSQL. Exercițiile de mai jos vă trec prin pașii necesari pentru a rula
un container pentru o bază de date PostgreSQL și containerul cu aplicația în aceeași rețea,
având persistență la oprirea containerelor.
:::

1. Pe baza surselor și a fișierului Dockerfile din arhiva de laborator, construiți o imagine cu
numele (tag-ul) <em>**api-laborator-1-image**</em>.
2. Creați o rețea bridge numită <em>**laborator1-db-network**</em>.
3. Creați un volum numit <em>**laborator1-db-persistent-volume**</em>.
4. Porniți în background un container pentru o bază de date cu următoarele caracteristici:
   - se va atașa un bind mount care va face o mapare între fișierul <em>**init-db.sql**</em> de pe
   mașina locală (acesta va fi sursa la flag-ul de bind mount și se găsește în arhiva de laborator)
   și fișierul <em>**/docker-entrypoint-initdb.d/init-db.sql**</em> din containerul care se va
   rula (acesta va fi destinația)
   - se va atașa volumul <em>**laborator1-db-persistent-volume**</em> creat anterior (sursa) la
   calea <em>**/var/lib/postgresql/data**</em> din containerul care se va rula (destinația)
   - se va rula containerul în rețeaua <em>**laborator1-db-network**</em> creată anterior
   - se vor specifica următoarele variabile de mediu (într-o comandă de <em>**docker run**</em>, acest
   se lucru se face astfel: <em>**docker run -e NUME=valoare**</em>):
      - variabila <em>**POSTGRES_USER**</em> cu valoare <em>**admin**</em>
      - variabila <em>**POSTGRES_PASSWORD**</em> cu valoarea <em>**admin**</em>
      - variabila <em>**POSTGRES_DB**</em> cu valoarea <em>**books**</em>
   - containerul rulat se va numi <em>**laborator1-db**</em>
   - se va rula imaginea <em>**postgres**</em> din registrul oficial.
5. Porniți în background un container cu imaginea <em>**api-laborator-1-image**</em> creată
anterior, cu următoarele caracteristici:
   - se va rula containerul în rețeaua <em>**laborator1-db-network**</em> creată anterior
   - se vor specifica următoarele variabile de mediu:
      - variabila <em>**PGUSER**</em> cu valoarea <em>**admin**</em>
      - variabila <em>**PGPASSWORD**</em> cu valoarea <em>**admin**</em>
      - variabila <em>**PGDATABASE**</em> cu valoarea <em>**books**</em>
      - variabila <em>**PGHOST**</em> cu valoarea <em>**laborator1-db**</em>
      - variabila <em>**PGPORT**</em> cu valoarea <em>**5432**</em>
   - containerul rulat se va numi <em>**laborator1-api**</em>
   - containerul va expune portul 80 și îl va mapa la portul 5555 de pe mașina locală.
6. Verificați că cele două containere rulează corect și au conectivitate:
   - folosind Postman sau orice altă aplicație similară, realizați cereri de GET și POST pe
   [http://localhost:5555/api/books](http://localhost:5555/api/books) (pentru un tutorial de
   Postman, puteți intra [aici](https://learning.postman.com/docs/getting-started/sending-the-first-request/))
   - la cererile de POST, se așteaptă un body JSON cu formatul `{"title":"titlu","author":"autor"}`
   - cererile de GET vor returna o listă de cărți adăugate prin cereri de POST.
7. Verificați că volumul pe care l-ați adăugat păstrează persistența datelor:
   - opriți și ștergeți cele două containere
   - reporniți cele două containere cu aceleași comenzi ca anterior
   - trimiteți o cerere de GET
   - dacă ați configurat corect, veți primi o listă cu cărțile adăugate anterior.