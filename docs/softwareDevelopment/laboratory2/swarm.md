---
title: Docker Swarm
sidebar_position: 2
---

În continuare, vom face tranziția de la Docker Compose la Docker Swarm,
<em>**orchestratorul de servicii**</em> oferit de Docker. Acesta are rolul de
a gestiona servicii de Docker pe una sau mai multe mașini într-o rețea (într-un
cluster) de mașini fizice și/sau virtuale. Spre deosebire de Docker Compose,
care rulează containere pe o singură gazdă, Docker Swarm rulează
<em>**servicii**</em> între mai multe gazde. La fel ca și Compose, Docker
Swarm folosește fișiere de configurare YAML.

## Arhitectura Docker Swarm

Docker Swarm se bazează pe algoritmul distribuit
[Raft](https://raft.github.io/), cu ajutorul căruia se menține consistentă
starea internă a întregului cluster. În plus, traficul dintre nodurile din
Swarm este criptat de către Raft. În imaginea de mai jos (preluată din
[documentația oficială](https://docs.docker.com/engine/swarm/how-swarm-mode-works/nodes/)),
se poate observa arhitectura unui cluster Docker Swarm.

<img alt="img" src="/img/softwareDevelopment/lab2_swarm-diagram.png" width="700" style={{margin: "auto", display: "block"}} />

Mașinile gazdă care fac parte dintr-un Swarm se numesc <em>**noduri**</em>
și pot avea două roluri:

- <em>**manager**</em> - rol administrativ și funcțional; menține consistența
clusterului, lansează în execuție servicii, expune endpoint-uri de rețea
- <em>**worker**</em> - rol funcțional; execută serviciile.

Dintre toate nodurile manager, un singur nod este <em>**leader**</em>, care
are rolul de a crea task-uri și de a face logging. Task-urile sunt distribuite
apoi nodurilor manager.

:::caution
Întotdeauna trebuie să existe un nod leader.
:::

:::tip
Deoarece toleranța este de (N-1)/2 noduri manager picate, este indicat să avem
un număr impar de noduri și un număr impar de manageri. Docker recomanda 3, 5
sau 7 manageri.
:::

## Crearea unui Docker Swarm

Odată ce avem un cluster de mașini pe care rulează Docker, ne putem
inițializa un Docker Swarm. Astfel, putem rula următoarea comandă pe nodul
care va fi leader (opțiunea <em>**--advertise-addr**</em> este necesară
atunci când nodul are mai multe interfețe de rețea și trebuie specificat pe
care din ele se face advertising):

```shell showLineNumbers
$ docker swarm init --advertise-addr 192.168.99.100
 
Swarm initialized: current node (qtyx0t5z275wp46wibcznx8g5) is now a manager.
To add a worker to this swarm, run the following command:
    docker swarm join --token SWMTKN-1-4hd41nyin8kn1wx4bscnnt3e98xtlvyxw578qwxijw65jp1a3q-32rl6525xriofd5xmv0c1k5vj 192.168.99.100:2377
To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

:::tip
Așa cum se poate observa, comanda de mai sus generează alte două comenzi pe
care le putem folosi pentru a introduce alte noduri în cluster, atât ca
worker, cât și ca manager.
:::

Putem verifica dacă swarm-ul a fost creat cu succes dând comanda de mai jos
pe mașina leader (unde avem două noduri numite <em>**node1**</em> și
<em>**node2**</em>, primul din ele fiind leader, iar al doilea worker):

```shell showLineNumbers
$ docker node ls
 
ID                            HOSTNAME     STATUS       AVAILABILITY      MANAGER STATUS
qtyx0t5z275wp46wibcznx8g5 *   node1        Ready        Active            Leader
0xbb9al1kuvn0jcapxiqni29z     node2        Ready        Active
```

## Servicii și stive de servicii Docker Swarm

Atunci când vorbim de deployment-ul unei aplicații în Docker Swarm, trecem de
la noțiunea de container la noțiunea de serviciu. Un serviciu Docker reprezintă
o colecție de task-uri (unul sau mai multe), iar un task reprezintă un
container. Așadar, un serviciu este format din unul sau mai multe containere
identice. Serviciul controlează ciclul de viață al containerelor, încercând
întotdeauna să mențină starea containerelor oferite în configurație. Cu alte
cuvinte, un serviciu reprezintă un set de containere cu
<em>**orchestrare**</em>.

Mai departe, o stivă de servicii reprezintă mai multe astfel de servicii
grupate în același spațiu de nume. Putem vizualiza o stivă de servicii ca
fiind o aplicație Docker formată din mai multe servicii. Cel mai facil mod
de a defini o stivă de servicii este prin intermediul unui fișier Docker
Compose, așa cum am văzut mai devreme. Comportamentul serviciilor dintr-o
stivă este similar cu cel al containerelor din Docker Compose, doar că
politica de denumire este diferită.

:::tip
Orice entitate creată într-o stivă (serviciu, volum, rețea, secret) va fi
prefixată de <em>**NUME-STIVA_**</em>.
:::

Docker Swarm are acces la o colecție nouă de opțiuni în cadrul fișierului
YAML de Compose, ce vor fi trecute în proprietatea
[deploy](https://docs.docker.com/compose/compose-file/compose-file-v3/#deploy)
a unui serviciu. Snippet-ul de mai jos prezintă un fragment de fișier Docker
Compose unde se exemplifică o parte din aceste opțiuni noi:

```yaml showLineNumbers
[...]
services:
  web:
    image: myimage
    deploy:
      replicas: 4
      resources:
        limits:
          cpus: "0.2"
          memory: 50M
      restart_policy:
        condition: on-failure
[...]
```

În fragmentul de fișier YAML de mai sus, se rulează un serviciu numit
<em>**web**</em> care are patru copii. Astfel, vor exista patru containere
diferite care rulează imaginea <em>**myimage**</em>, oricare din ele putând
răspunde la cereri pentru serviciul <em>**web**</em>, în funcție de încărcare.
De asemenea, fiecare instanță este limitată la 20% CPU (pe toate core-urile) și
50 MB de RAM. Nu în ultimul rând, un container al serviciului <em>**web**</em>
se restartează imediat ce întâlnește o eroare (scopul final fiind ca, la orice
moment de timp, să existe 4 copii ale containerului în rețea).

### Rețele Swarm

Spre deosebire de Docker clasic și Compose, rețelele create în Swarm nu mai
sunt de tip bridge, ci de tip <em>**overlay**</em>. O rețea de tip overlay
este o rețea care se întinde peste toate nodurile dintr-un swarm. Din acest
motiv, porturile publice expuse vor fi unice per rețea. Așadar, nu pot fi
expuse două porturi 3000 din două servicii diferite care se conectează la
aceeași rețea overlay.

:::tip
Docker Swarm realizează balansare a încărcării la nivelul rețelei.
:::

Un serviciu la care s-a făcut deploy pe un anumit port totdeauna va avea acel
port rezervat, indiferent pe ce nod rulează de fapt containerul sau
containerele sale. Diagrama de mai jos (preluată din
[documentația oficială](https://docs.docker.com/engine/swarm/ingress/))
prezintă o situație în care avem un serviciu numit <em>**my-web**</em>
publicat pe portul 8080 într-un cluster cu trei noduri. Se poate observa că,
dacă ne conectăm pe portul 8080 de pe oricare adresă IP de nod din cluster, vom
fi redirecționați către un container care rulează serviciul specific portului
extern 8080, indiferent de nodul pe care rulează.

<img alt="img" src="/img/softwareDevelopment/lab2_network.png" width="700" style={{margin: "auto", display: "block"}} />

### Secrete Swarm

Secretele din Swarm trebuie create înainte de a fi rulată configurația.
Putem folosi următoarea comandă:

```shell showLineNumbers
$ docker secret create mysecret file.txt
 
fm49ig0i8x9pdq0xxa8wdchoy
```

Putem lista secretele existente astfel:

```shell showLineNumbers
$ docker secret ls
 
ID                          NAME       DRIVER    CREATED         UPDATED
fm49ig0i8x9pdq0xxa8wdchoy   mysecret             3 seconds ago   3 seconds ago
```

Secretele din Swarm sunt criptate în Raft și deci este recomandat să fie folosite în producție.

### Diferențe între Docker Swarm și Docker Compose

Exista câteva diferențe între Swarm și Compose la nivelul fișierelor
declarative YAML:

- deoarece Swarm rulează servicii în rețea, nu poate exista
cuvântul-cheie <em>**build**</em>; serviciile trebuie obligatoriu să
fie rulate pe baza unor imagini deja existente într-un registru
- stivele de servicii nu accepta fișiere <em>**.env**</em> (spre deosebire
de Docker Compose)
- Docker Compose rulează containere în mod single-host, pe când Docker
Swarm orchestrează servicii în mod multi-host.

### Pornirea unei stive de servicii în Docker Swarm

Odată ce swarm-ul Docker a fost creat și inițializat, comanda prin care se
face deployment la o stivă de servicii este următoarea (unde configurația
se găsește în fișierul <em>**my_stack.yml**</em>, iar numele stivei va fi
<em>**lab2**</em>):

```shell showLineNumbers
$ docker stack deploy -c my_stack.yml lab2
```

:::caution
Dacă folosim un registru diferit de Docker (precum
[GitLab Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/)),
este nevoie să fim autentificați și să adăugăm opțiunea
<em>**-–with-registry-auth**</em> atunci când lansăm o stivă de servicii.
:::

Odată ce o stivă de servicii a fost pornită, putem să îi vedem statusul prin următoarea comandă:

```shell showLineNumbers
$ docker stack ps lab2

ID             NAME                   IMAGE                                   NODE      DESIRED STATE    CURRENT STATE           ERROR               PORTS
cuktma92gm62   lab2_adminer.1         adminer:latest                          myvm2     Running          Running 9 minutes ago                       
njak2qzaobtt   lab2_db.1              postgres:12                             myvm1     Running          Running 8 minutes ago                       
m811buil7e63   lab2_io-service.1      mobylab/idp-laborator2-io:latest        myvm1     Running          Running 9 minutes ago                       
jnfw37e34kz3   lab2_books-service.1   mobylab/idp-laborator2-books:latest     myvm1     Running          Running 9 minutes ago                       
pzlzkgsxxc00   lab2_gateway.1         mobylab/idp-laborator2-gateway:latest   myvm2     Running          Running 9 minutes ago                       
kpaahb931rbq   lab2_io-service.2      mobylab/idp-laborator2-io:latest        myvm1     Running          Running 9 minutes ago                       
num87yijgxrg   lab2_books-service.2   mobylab/idp-laborator2-books:latest     myvm2     Running          Running 9 minutes ago                       
d9m63k9h7ium   lab2_gateway.2         mobylab/idp-laborator2-gateway:latest   myvm1     Running          Running 9 minutes ago                       
lkmy60wpy0gv   lab2_io-service.3      mobylab/idp-laborator2-io:latest        myvm2     Running          Running 9 minutes ago                       
fy21iizn0reb   lab2_gateway.3         mobylab/idp-laborator2-gateway:latest   myvm2     Running          Running 9 minutes ago
```

De asemenea, putem vedea lista de stive pornite astfel:

```shell showLineNumbers
$ docker stack ls

NAME      SERVICES      ORCHESTRATOR
lab2      5             Swarm
```

Putem vedea lista de servicii (din toate stivele pornite) astfel:

```shell showLineNumbers
$ docker service ls

ID               NAME                 MODE           REPLICAS               IMAGE                                  PORTS
dekzzyais8g7     lab2_adminer         replicated     1/1                    adminer:latest                         *:8080->8080/tcp
74y84hvq4irn     lab2_books-service   replicated     2/2 (max 1 per node)   mobylab/idp-laborator2-books:latest     
ns9mxet1rkx5     lab2_db              replicated     1/1                    postgres:12                             
dh3sv3q74fy6     lab2_gateway         replicated     3/3 (max 2 per node)   mobylab/idp-laborator2-gateway:latest  *:3000->80/tcp
ru0rd7g2ypu8     lab2_io-service      replicated     3/3 (max 2 per node)   mobylab/idp-laborator2-io:latest
```

## Deployment-ul unui cluster Docker

În cadrul laboratoarelor de IDP, avem două variante principale cu ajutorul
cărora putem să ne creăm un cluster Docker format din mai multe mașini:
[Docker Machine](https://docs.docker.com/machine/) sau
[Play with Docker](https://labs.play-with-docker.com/).

### Docker Machine

:::caution
Recent, Docker Machine a devenit deprecated, deci cei de la Docker nu mai
mențin acest proiect. Puteți totuși testa un deployment multi-nod folosind
Docker Machine, dar vă recomandăm varianta Play with Docker sau un cluster
propriu (fizic sau virtual).
:::

Docker Machine este un utilitar care permite rularea Docker Engine pe gazde
virtuale (atât local, cât și în cloud, la provideri ca AWS, Azure sau
DigitalOcean), precum și gestiunea facilă a acestora din terminal. Docker
Machine trebuie instalat separat de Docker, conform
[documentației oficiale](https://docs.docker.com/machine/install-machine/).

În Docker Machine, putem folosi drivere pentru a crea noduri virtuale
configurate pentru a rula Docker. Exista drivere pentru a crea noduri direct în
cloud, dar și drivere pentru a crea mașini virtuale pe gazda locală. În
exemplul de mai jos, se creează un nod virtual local folosind driverul pentru
VirtualBox:

```shell showLineNumbers
$ docker-machine create --driver virtualbox myvm1

Running pre-create checks...
Creating machine...
(myvm1) Copying /home/radu/.docker/machine/cache/boot2docker.iso to /home/radu/.docker/machine/machines/myvm1/boot2docker.iso...
(myvm1) Creating VirtualBox VM...
(myvm1) Creating SSH key...
(myvm1) Starting the VM...
(myvm1) Check network to re-create if needed...
(myvm1) Waiting for an IP...
Waiting for machine to be running, this may take a few minutes...
Detecting operating system of created instance...
Waiting for SSH to be available...
Detecting the provisioner...
Provisioning with boot2docker...
Copying certs to the local machine directory...
Copying certs to the remote machine...
Setting Docker configuration on the remote daemon...
Checking connection to Docker...
Docker is up and running!
To see how to connect your Docker Client to the Docker Engine running on this virtual machine, run: docker-machine env myvm1
```

Putem verifica rularea corectă a comenzilor și alte informații utile despre
starea nodurilor virtuale astfel:

```shell showLineNumbers
$ docker-machine ls
NAME    ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER        ERRORS
myvm1   -        virtualbox   Running   tcp://192.168.99.100:2376           v17.09.0-ce
```

Prin comanda <em>**docker-machine ssh**</em>, putem trimite comenzi prin SSH
către un nod virtual Docker pe care îl specificăm prin nume, așa cum se poate
observa mai jos.

```shell showLineNumbers
$ docker-machine ssh myvm1 "ls -la"
```

În mod similar, există și comanda <em>**docker-machine scp**</em>:

```shell showLineNumbers
$ docker-machine scp file.txt myvm1:.
```

De asemenea, există și comenzi pentru oprirea, respectiv ștergerea, nodurilor
virtuale create.

```shell showLineNumbers
$ docker-machine stop myvm1
```

```shell showLineNumbers
$ docker-machine rm myvm1
```

### Play with Docker

[Play with Docker](https://www.docker.com/play-with-docker/) este un mediu
online de învățare Docker ce oferă mașini virtuale pe o durată de 4 ore.

<img alt="img" src="/img/softwareDevelopment/lab2_playwithdocker.png" width="700" style={{margin: "auto", display: "block"}} />

## Comenzi utile

### Interacțiune servicii

:::tip
Aceste comenzi pot fi rulate doar de pe noduri manager.
:::

```shell showLineNumbers
$ docker service create --name <NUME_SERVICIU> <IMAGINE_DOCKER> # creează un serviciu pornind de la o imagine
$ docker service ls                                             # afișează toate serviciile din sistem
$ docker service inspect <NUME_SERVICIU>                        # afișează informații despre un serviciu
$ docker service logs –f <NUME_SERVICIU>                        # afișează log-urile unui serviciu
$ docker service ps <NUME_SERVICIU>                             # afișează task-urile (și statusurile lor) pentru un serviciu
$ docker service update --replicas <N> <NUME_SERVICIU>          # actualizează serviciul, replicând containerele de N ori
$ docker service rm <NUME_SERVICIU>                             # șterge un serviciu
```

### Interacțiune cluster

:::tip
Aceste comenzi pot fi rulate doar de pe noduri manager.
:::

```shell showLineNumbers
$ docker node ls                            # afișează nodurile din cluster
$ docker node promote <NUME_NOD>            # promovează nodul din worker în manager
$ docker node demote <NUME_NOD>             # retrogradează nodul din manager în worker
$ docker swarm init [--advertise-addr <IP>] # creează un cluster Docker
$ docker swarm join --token <TOKEN> <IP>    # se alătură unui cluster Docker
```

### Docker Machine

```shell showLineNumbers
$ docker-machine create [--driver <DRIVER>] <NUME> # creează o mașină virtuală Docker
$ docker-machine start <NUME>                      # pornește o mașină virtuală Docker
$ docker-machine stop <NUME>                       # oprește o mașină virtuală Docker
$ docker-machine rm <NUME>                         # șterge o mașină virtuală Docker
$ docker-machine ls                                # listează toate mașinile virtuale Docker
$ docker-machine ssh <NUME>                        # se conectează prin SSH la o mașină virtuală Docker
$ docker-machine scp <FISIER> <NUME>:<DESTINATIE>  # copiază un fișier pe o mașină virtuală Docker
```