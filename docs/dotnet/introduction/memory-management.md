---
title: Gestiunea automata a memoriei
sidebar_position: 4
---

# Despre gestiunea automata a memoriei

In general exista doua metode de prin care se poate realiza gesiunea automata a memoriei in limbaje de programare, **garbage collection (GC)** si **smart pointers**.

Smart pointers sunt structuri de date speciale care tin evidenta numarului de referinte a unui obiect alocat dinamic, in general prin numarare de referinte la copierea structurii si la distrugerea acesteia. In limbaje cum e C++ aceste structuri in interior folosesc principiu **RAII (Resource Acquisition Is Initialization)** pentru a gestiona referintele alocate dinamic. Pot fi smart pointeri partajati (shared) care numara referintele si poate fi partajat de mai multi detinatori (owners) sau unici (unique) unde doar un detinator poate avea referinta la un moment dat. 

Alternativa la smart pointeri e garbage collection, la runtime o runtina a programului numit **garbage collector** urmareste referintele si atat timp cat referintele sunt accesibile de la o referinta de pe stack nu se va recicla memoria pentru acele referinte. Altfel, se vor distruge obiectele si memoria dezalocata. Limbaje cu garbage collection sunt foarte populare, cum e C#, Java si Go.

Pentru ambele solutii exista avantaje si dezavantaje:

|          | Avantaje | Dezavantaje |
|:--------------|:------------:|:---------------:|
| Smart pointers | Timpii de rulare ai programului sunt deterministi | Pot fi folositi in mod gresit in functie de specializare |
| | Performanta mai buna la rulare fata de GC ca timp/memorie consumata | Este mai greu de invatat folosilea lor in functie de cazul de utilizare |
| | Mai usor de implementat ca GC | Daca se folosesc si pointeri normali (usafe) pot aparea in continuare probleme |
| Garbage collection| Programatorul nu trebuie sa sesizeze prezenta de GC | Timpii de rulare ai programului sunt nedeterministi |
| | E mult mai greu de a face memory-leaking | Nu este imposibil de a face memory-leaking in special daca se uita fire de executie deschise |
| | | Este mult mai greu de implementat ca smart pointers |