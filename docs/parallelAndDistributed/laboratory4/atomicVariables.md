---
title: Variabile atomice
sidebar_position: 5
---

În cadrul laboratorului 2, am observat cum operația de incrementare (exemplul cu instrucțiunea a+=2) nu este o operație atomică (operație ce nu poate fi divizată atunci când este executată de către un thread). Java oferă suport pentru o serie de tipuri de date (tipuri atomice) ce au asociate operații atomice (mai multe detalii [aici](https://docs.oracle.com/javase/8/docs/api/?java/util/concurrent/atomic/package-summary.html) și [aici](https://docs.oracle.com/javase/tutorial/essential/concurrency/atomicvars.html)). Acestea sunt utile pentru a fi folosite pe post de contoare sau acumulatori fără a mai folosi mecanisme de sincronizare.