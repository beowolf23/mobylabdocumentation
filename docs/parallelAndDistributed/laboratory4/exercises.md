---
title: Exerciții
sidebar_position: 6
---

1. Creați un program care să lanseze un număr de thread-uri egal cu numărul de core-uri de care dispune calculatorul vostru. Fiecare thread trebuie să afișeze la consolă un text de tipul "Hello from thread #id".

2. Rezolvați bug-urile prezente în [scheletul de laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab04). Folosiți-vă de **sugestiile** din surse.

3. Paralelizați dublarea elementelor unui vector, plecând de la scheletul de laborator, unde aveți versiunea serială.

4. Paralelizați algoritmul Floyd-Warshall, plecând de la scheletul de laborator, unde aveți versiunea serială.