---
title: Send
sidebar_position: 1
---

## MPI_Send

MPI_Send reprezintă funcția prin care un proces trimite date către un alt proces. Semnătura funcției este următoarea:

```c showLineNumbers
int MPI_Send(void* data, int count, MPI_Datatype datatype, int destination, int tag, MPI_Comm communicator)
```

Unde:

* **data** (↓) - reprezintă datele trimise de la procesul sursă către procesul destinație
* **count** (↓) - dimensiunea datelor transmise
* **datatype** (↓) - tipul datelor transmise
* **destination** (↓) - rangul / identificatorului procesului destinație, către care se trimit datele
* **tag** (↓) - identificator al mesajului
* **communicator** (↓) - comunicatorul în cadrul căruia se face trimiterea datelor între cele două procese

MPI_Send este o funcție blocantă. Mai precis, programul se blochează până când bufferul dat ca prim parametru poate fi refolosit, chiar dacă nu se execută acțiunea de primire a mesajului transmis de procesul curent (MPI_Recv). Dacă apare cazul în care procesul P1 trimite date (MPI_Send) la procesul P2, iar P2 nu are suficient loc în buffer-ul de recepție (buffer-ul nu are suficient loc liber sau este plin) atunci P1 se va bloca.