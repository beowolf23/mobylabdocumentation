---
title: Organizarea unui cluster
sidebar_position: 1
---

În cazul unui sistem distribuit, elementul cheie este **comunicarea** între procese. Aceasta dictează performanța sistemului, toleranța la defecte și logica fluxului de transfer de informații între nodurile din cadrul sistemului distribuit.

Din punct de vedere al arhitecturii, nodurile pot fi organizate în mai multe moduri într-un cluster:

* grid → nodurile sunt organizate pe nivele. Fiecare nod poate comunică cu vecinii pe orizontală și pe verticală. Fluxul informației este vertical și orizontal. Fiecare nod are maxim 4 vecini.
* inel → nodurile sunt organizate în cerc. Fiecare nod vorbește cu vecinii din stânga și din dreapta. Fluxul informației este orizontal. Fiecare nod are doi vecini.
* arbore → nodurile sunt organizate ierarhic, sub forma de arbore. Fluxul informației este vertical. Fiecare nod poate avea un număr nelimitat de vecini.
* graf → nodurile sunt organizate sub forma unui graf. Fiecare nod poate comunica cu unul sau mai mulți vecini. Fluxul informației este abstract. Fiecare nod poate avea un număr nelimitat de vecini.

În cadrul acestui laborator vom explora organizarea unui sistem sub forma de graf generic. Topologia de mai jos va fi folosită pentru exemplificări în cadrul laboratorului și în cadrul exercițiilor.

<img alt="img" src="/img/parallel-and-distributed/distributedTopology.png" width="75%" style={{margin: "auto", display: "block"}} />

## Stabilirea unei topologii

Există trei moduri prin care o topologie poate fi stabilită în cadrul unui cluster. Nodurile pot cunoaște starea întregului cluster de la bun început, pot cunoaște doar vecinii sau nu pot ști nimic despre starea sistemului.

Pentru a stabili topologia, clusterul poate opera în două moduri:

* nodurile pot transmite starea lor celorlalte noduri de mai multe ori până când toate nodurile au topologia stabilită. Numărul de dăți necesar asigurării topologiei între noduri se numește **convergență**. Acest mod este simplu de realizat, dar ineficient
* nodurile aleg un **lider** care să calculeze topologia sistemului

Problema care se ridică în cazul utilizării unui lider care să creeze matricea de topologie, este că nodurile nu știu calea către lider, ci doar cine este acest lider. **Arborele de acoperire** rezolvă această problemă. Pe baza arborelui de acoperire, nodurile trimit informația pe care o dețin către lider, trecând prin nodurile intermediare care sunt reprezentate de părinții lor în arbore. Nodurile intermediare adăugă informația primită la informația proprie și o trimit mai departe. În final, liderul are toată informația sistemului și poate crea matricea de topologie. Matricea este trimisă, apoi, fiecărui nod în parte.

În cadrul acestui laborator vom explora stabilirea topologiei prin alegerea liderului.

:::tip
În cazul rețelelor de calculatoare, router-ele și switch-urile sunt organizate sub forma unui cluster unde nodurile nu știu nimic despre starea sistemului la început.
:::

:::caution
În cazul alegerii liderului, nu este nevoie să se cunoască topologia la început, ci doar vecinii cu care nodurile pot interacționa.
:::