---
title: Exerciții
sidebar_position: 4
---

Pornind de la [scheletul de laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab10), va trebui să realizați următoarele sarcini:

1. Aflați nodul lider al clusterului folosind un algoritm *heartbeat* (funcția `leader_chosing` din schelet).
2. Realizați arborele de acoperire, plecând din lider, folosind un algoritm *undă-ecou* (funcția `get_dst` din schelet).
3. Asigurați-vă ca numărul de elemente din arborele de acoperire a fost stabilit bine prin calcularea numărului de noduri folosind un algoritm *epidemic* (funcția `get_number_of_nodes` din schelet).
4. Folosind arborele de acoperire, trimiteți către lider configurația fiecărui nod și realizați în lider matricea de topologie a clusterului. Distribuiți topologia către toate nodurile (funcția `get_topology` din schelet).