---
title: Reduce
sidebar_position: 1
---

Operația de reduce (cunoscută în limbajele funcționale - Haskell, Racket - drept fold) reprezintă un tip de operație prin care elementele unei colecții sunt restrânse / acumulate într-un singur rezultat, printr-o singură operație aplicată între elementele unei colecții (+, *, min, max etc.)

Exemplu:

```c showLineNumbers
l = [1, 2, 3, 4, 5, 6]
op = +
rezultat = 1 + 2 + 3 + 4 + 5 + 6 = 21
```

În MPI, operația reduce este implementată în funcția MPI_Reduce, însă în cadrul laboratorului vom discuta cum poate fi implementată, de mână, această operație în MPI.

Aici aveți atașate slide-uri, care descriu în detaliu pașii de implementare ai operației reduce: [slides](/files/parallel-and-distributed/reduce.pdf)

În concluzie, primul proces va avea valoarea finală.

Pseudocod:

```c showLineNumbers
for (pas = 2; pas <= nr_procese; pas *= 2)
	if (rank % pas == 0)
		primește de la procesul cu rank-ul [rank + (pas / 2)]
		adună
        else if (rank % (pas / 2) == 0)
                trimite la procesul cu rank-ul [rank - (pas / 2)]
```