---
title: Problema bărbierului
sidebar_position: 3
---

Avem următoarea situație: avem o frizerie cu un bărbier (un thread), un scaun de bărbier, N scaune de așteptare și M clienți (M thread-uri).

La această problemă avem următoarele constrângeri:

* bărbierul doarme atunci când nu sunt clienți
* când vine un client, acesta fie trezește bărbierul, fie așteaptă dacă bărbierul este ocupat
* dacă toate scaunele sunt ocupate, clientul pleacă

Pseudocod:

```c showLineNumbers
int freeChairs = N;
semaphore clients(0);
semaphore barber_ready(0);
semaphore chairs(1); // sau mutex

barber() {
    while(true) {
        clients.acquire(); // se caută client; dacă există, el este chemat
		
	    chairs.acquire(); // are client, un scaun este eliberat, modificăm freeChairs
		
	    freeChairs++; // scaun eliberat

	    barber_ready.release(); // bărbierul e gata să tundă
	    chairs.release(); // freeChairs modificat

	    // tunde bărbierul
    }
}

client(int id) {
    while(true) {
        chairs.acquire(); // vine un client și caută un scaun liber
        if (freeChairs > 0) {
	        freeChairs--; // clientul a găsit scaun
			
	        clients.release(); // bărbierul știe că s-a ocupat un scaun de un client
			
	        chairs.release(); // freeChairs modificat
			
	        barber_ready.acquire(); // clientul își așteaptă rândul la tuns
	    } else {
	        // nu sunt scaune libere
	        chairs.release();
	        // clientul pleacă netuns
	    }
    }
}
```