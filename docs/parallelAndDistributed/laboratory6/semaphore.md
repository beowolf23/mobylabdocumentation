---
title: Semafoare (continuare)
sidebar_position: 1
---

Laboratorul trecut, am văzut cum semaforul reprezintă o generalizare a unui mutex. Un semafor iniţializat cu 1 poate fi folosit drept lock, pentru că doar un thread are acces la zona critică la un moment de timp. Totuși, un semafor poate avea întrebuinţări mult mai complexe, deoarece poate lua diverse valori, atât negative, cât şi pozitive.

Când un semafor este iniţializat cu valoarea pozitivă *x*, vă puteţi gândi că *x* thread-uri au voie să intre în secţiunea critică. Pe măsură ce un thread face **acquire()**, *x* este decrementat. Când se ajunge la 0, alte posibile thread-uri care vor să acceseze regiunea critică vor trebui să aştepte până când valoarea semaforului creşte la o valoare pozitivă (adică până când iese câte un thread din cele aflate în regiunea critică).

Complementar, când un semafor este iniţializat cu o valoare negativă cum ar fi -1, se aşteaptă ca cel puţin două thread-uri să facă întâi **release()** (pentru ca valoarea semaforului să crească de la -1 la 1), înainte ca o regiune critică să poată fi accesată (adică un alt thread să poată face **acquire()**). Vă puteţi imagina că un al treilea thread aşteaptă "la semafor" ca alte două thread-uri să îi dea un semnal prin apelul **release()** când şi-au "terminat treaba". Puteți observa un exemplu în pseudocodul de mai jos.

<table>
    <thead>
        <tr>
            <td colSpan="3">
                Semaphore sem = new Semaphore(-1);
            </td>
        </tr>
        <tr>
            <th>T0</th>
            <th>T1</th>
            <th>T2</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                // aşteaptă la semafor
                <br />
                // după celelalte 2 thread-uri
                <br />
                sem.acquire();
                <br />
                <br />
                <br />
                <br />
                <br />
                // sem = 1, deci poate trece
                <br />
                System.out.println("Am trecut de semafor!");
            </td>
            <td>
                <br />
                do_work1();
                <br />
                sem.release();
                <br />
                // sem = -1 + 1 = 0
                <br />
                <br />
                <br />
                <br />
            </td>
            <td>
                <br />
                <br />
                <br />
                <br />
                do_work2();
                <br />
                sem.release();
                <br />
                // sem = 0 + 1 = 1
                <br />
            </td>
        </tr>
    </tbody>
</table>