---
title: Exerciții
sidebar_position: 7
---

1. Pornind de la [scheletul de laborator](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab06), rezolvați problema de sincronizare din pachetul *multipleProducersMultipleConsumers*, folosind ArrayBlockingQueue.
2. Rezolvați problema de sincronizare din pachetul *synchronizationProblem*, folosind un AtomicInteger.
3. Rezolvați problema de sincronizare din pachetul *bugConcurrentHashMap*, folosind metode din clasa ConcurrentHashMap.
4. Paralelizați programul din pachetul *synchronizedSortedList*, unde:
    * trei thread-uri citesc numere din trei fișiere (câte un thread citește câte un fișier) și adaugă numerele din fișiere într-o listă partajată.
    * al patrulea thread sortează lista cu numerele.
    * lista poate fi sortată **doar** după ce citirea a fost efectuată de către toate cele trei thread-uri care citesc din fișier.
    * sincronizarea între thread-uri trebuie realizată folosind un semafor.
5. Paralelizați programul din pachetul *parallelTree*, unde:
   * două thread-uri construiesc un arbore binar în paralel, citind ID-urile și părintele nodurilor noi din câte un fișier, și apoi executând operația de inserare
   * al treilea verifică dacă arborele a fost construit corect
   * verificarea arborelui nu se poate face decât după finalizarea alcătuirii sale
   * sincronizarea între thread-uri trebuie realizată folosind o barieră
   * la inserția în arbore, sincronizarea nu trebuie făcută global (pe tot arborele), ci pentru fiecare nod din arbore.