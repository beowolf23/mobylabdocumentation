---
title: Metodele Collections.synchronized
sidebar_position: 5
---

Dacă dorim să lucrăm cu colecții clasice, precum ArrayList, și să avem operațiile sincronizate, putem să le instanțiem folosind:

* Collections.synchronizedList (pentru liste):

```java showLineNumbers
List<Integer> syncList = Collections.synchronizedList(new ArrayList<>());
```

* Collections.synchronizedMap (pentru dicționare):

```java showLineNumbers
Map<Integer, String> syncMap = Collections.synchronizedMap(new HashMap<>());
```