---
title: Noțiuni generale
sidebar_position: 1
---

## Imagini și containere

Containerele Docker au la bază imagini, care sunt pachete executabile lightweight
de sine stătătoare ce conțin tot ce este necesar pentru rularea unor aplicații software,
incluzând cod, runtime, biblioteci, variabile de mediu și fișiere de configurare.
Imaginile au o dimensiune variabilă, nu conțin versiuni complete ale sistemelor de operare,
și sunt stocate în cache-ul local sau într-un registru. O imagine Docker are un sistem de
fișiere de tip <em>**union**</em>, unde fiecare schimbare asupra sistemului de fișiere sau metadate este
considerată ca fiind un strat (layer), mai multe astfel de straturi formând o imagine.
Fiecare strat este identificat unic (printr-un hash) și stocat doar o singură dată.

Un container reprezintă o instanță a unei imagini, adică ceea ce imaginea devine în
memorie atunci când este executată. El rulează complet izolat de mediul gazdă, accesând
fișiere și porturi ale acestuia doar dacă este configurat să facă acest lucru.
Containerele rulează aplicații nativ pe nucleul mașinii gazdă, având performanțe mai
bune decât mașinile virtuale, care au acces la resursele gazdei prin intermediul unui
hipervizor. Fiecare container rulează într-un proces discret, necesitând tot atât de
multă memorie cât orice alt executabil. Din punct de vedere al sistemului de fișiere,
un container reprezintă un strat adițional de read/write peste straturile imaginii.

<img alt="img" src="/img/softwareDevelopment/lab1_vmcontainer.png" width="600" style={{margin: "auto", display: "block"}} />

În imaginea de mai sus (preluată din [documentația oficială Docker](https://docs.docker.com/get-started/)),
mașinile virtuale rulează sisteme de operare „oaspete”, lucru care consumă multe resurse, iar imaginea
rezultată ocupă mult spațiu, conținând setări de sistem de operare, dependențe, patch-uri de securitate,
etc. În schimb, containerele pot să împartă același nucleu, și singurele date care trebuie să fie într-o
imagine de container sunt executabilul și pachetele de care depinde, care nu trebuie deloc instalate pe
sistemul gazdă. Dacă o mașină virtuală abstractizează resursele hardware, un container Docker este un
proces care abstractizează baza pe care rulează aplicațiile în cadrul unui sistem de operare și izolează
resursele software ale sistemului de operare (memorie, access la rețea și fișiere, etc.).

## Arhitectura Docker

Docker are o arhitectură de tip client-server, așa cum se poate observa în imaginea de mai jos
(preluată din [documentația oficială Docker](https://docs.docker.com/get-started/)). Clientul Docker
comunică, prin intermediul unui API REST (peste sockeți UNIX sau peste o interfață de rețea), cu
daemon-ul de Docker (serverul), care se ocupă de crearea, rularea și distribuția de containere Docker.
Clientul și daemon-ul pot rula pe același sistem sau pe sisteme diferite. Un registru Docker are rolul
de a stoca imagini.

<img alt="img" src="/img/softwareDevelopment/lab1_architecture.png" width="700" style={{margin: "auto", display: "block"}} />