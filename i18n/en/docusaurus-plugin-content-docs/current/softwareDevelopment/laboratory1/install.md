---
title: Instalarea și testarea instalării
sidebar_position: 2
---

## Instalarea

Docker este disponibil în două variante: Community Edition (CE) și Enterprise Edition (EE). Docker CE
este util pentru dezvoltatori și echipe mici care vor să construiască aplicații bazate pe containere.
Pe de altă parte, Docker EE a fost creat pentru dezvoltare enterprise și echipe IT care scriu și
rulează aplicații critice de business pe scară largă. Versiunea Docker CE este gratuită, pe când EE
este disponibilă cu subscripție. În cadrul laboratorului de IDP, vom folosi Docker Community Edition.
Docker este disponibil atât pe platforme desktop (Windows, macOS), cât și Cloud (Amazon Web Services,
Microsoft Azure) sau server (CentOS, Fedora, Ubuntu, Windows Server 2016, etc.).

### Linux

Comenzile de mai jos sunt pentru Ubuntu. Pentru alte variante de Linux (Debian, CentOS, Fedora), găsiți
informații suplimentare pe pagina de documentație oficială Docker.

Pentru instalarea Docker CE, este nevoie de una din următoarele versiuni de Ubuntu: Ubuntu Mantic
23.10, Ubuntu Jammy 22.04 (LTS), Ubuntu Focal 20.04 (LTS). Docker CE are suport pentru arhitecturile <em>**x86_64**</em>, <em>**amd64**</em>, <em>**armhf**</em>, <em>**arm64**</em>, <em>**s390x**</em>, și <em>**ppc64le**</em> (<em>**ppc64el**</em>).

Varianta recomandată de instalare a Docker CE presupune folosirea repository-ului oficial, deoarece
update-urile sunt apoi instalate automat. La prima instalare a Docker CE pe o mașină, este necesară
inițializarea repository-ului:

```shell showLineNumbers
$ sudo apt-get update
```

```shell showLineNumbers
$ sudo apt-get install ca-certificates curl gnupg lsb-release
```

```shell showLineNumbers
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

```shell showLineNumbers
$ echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

După inițializare, se poate instala Docker CE:

```shell showLineNumbers
$ sudo apt-get update
```

```shell showLineNumbers
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

:::tip
O variantă mai simplă de a instala Docker CE pe Linux este utilizarea [acestui script](https://get.docker.com/).
:::

### Windows și MacOS

Pentru că Docker nu avea inițial suport nativ pentru Windows și MacOS, s-a introdus
[Docker Toolbox](https://docs.docker.com/toolbox/overview/), care poate lansa un mediu Docker
virtualizat (mai precis, se folosește o mașină virtuală VirtualBox pentru a fi baza mediului
de Docker). De câțiva ani, Docker Toolbox a fost marcat ca „legacy” și a fost înlocuit de
[Docker Desktop for Mac](https://docs.docker.com/docker-for-mac/) și
[Docker Desktop for Windows](https://docs.docker.com/docker-for-windows/),
care oferă funcționalități similare cu performanțe mai bune. Mai mult, Windows Server 2016, Windows
10 și Windows 11 au acum suport pentru Docker nativ pentru arhitecturi <em>**x86_64**</em>.

## Testarea instalării

Pentru a verifica dacă instalarea s-a realizat cu succes, putem rula un container simplu
de tip Hello World:

```shell showLineNumbers
$ docker container run hello-world

Unable to find image 'hello-world:latest' locally
latest: Pulling from library/hello-world
c1ec31eb5944: Pull complete 
Digest: sha256:d000bc569937abbe195e20322a0bde6b2922d805332fd6d8a68b19f524b7d21d
Status: Downloaded newer image for hello-world:latest

Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```

Output-ul execuției ne arată pașii pe care Docker îi face în spate pentru a rula acest container. Mai
precis, dacă imaginea pe care dorim să o rulăm într-un container nu este disponibilă local, ea este
descărcată din repository, după care se creează un nou container pe baza acelei imagini, în care se
rulează aplicația dorită.