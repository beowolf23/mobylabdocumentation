---
title: Docker pe Windows și pe MacOS
sidebar_position: 6
---

Docker a fost creat nativ pentru Linux, utilizând componente de kernel specifice Linux, cum ar
fi <em>**cgroups**</em> sau <em>**namespaces**</em>, folosite pentru a izola procese și alte
componente ale sistemului de operare. Începând din 2016, el poate rula nativ și pe Windows, dar
doar pentru versiunile Windows Server 2016, Windows 10 și Windows 11. De aceea, pentru a rula pe
un sistem de operare desktop precum un Windows mai vechi sau MacOS, Docker necesită rularea
virtualizată.

Pe Windows, se folosește izolare Hyper-V pentru a rula un kernel Linux cu un set minimal de
componente suficiente pentru a executa Docker. Pe MacOS, Docker for Mac este o aplicație nativă
care conține un hypervizor bazat pe [xhyve](https://github.com/machyve/xhyve) și o distribuție
minimală de Linux, peste care rulează Docker. Astfel, se oferă o experiență mult mai apropiată
de utilizarea Docker pe Linux, sistemul de operare pentru care a fost creat.

Ca un exemplu, pentru a avea acces în mașina virtuală de Docker pentru MacOS, se poate folosi
<em>**screen**</em> (pentru a se termina sesiunea, se folosește combinația de taste
<em>**Ctrl+a, k**</em>):

```shell showLineNumbers
$ screen /Users/<UID>/Library/Containers/com.docker.docker/Data/vms/0/tty
 
linuxkit-025000000001:~# pwd
/root$ docker image pull alpine
```

O alternativă mai elegantă la comanda de <em>**screen**</em> de mai sus este utilizarea unei
imagini speciale pentru accesul în mașina virtuală Docker:

```shell showLineNumbers
$ docker run -it --privileged --pid=host justincormack/nsenter1
