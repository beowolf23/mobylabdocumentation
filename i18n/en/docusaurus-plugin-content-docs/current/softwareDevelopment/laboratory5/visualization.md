---
title: Vizualizare
sidebar_position: 3
---

Așa cum am importat Loki ca sursă de date în Grafana pentru a realiza query-uri și a
avea o interfață grafică ușor de utilizat, același lucru îl putem face și pentru
Prometheus. Dacă, până acum, se generau date care să fie afișate în Prometheus, în
cazul de față Prometheus funcționează ca sursă de date, iar Grafana va primi datele și
le va afișa într-un dashboard.

Tot ce trebuie făcut în Docker Compose este să punem Grafana și Prometheus în aceeași
rețea, iar apoi să intrăm pe dashboard la [http://IP:3000/](http://<IP>:3000/) și să
adăugăm noua sursă de date, similar cu Loki (URL-ul sursei fiind de această dată
[http://prometheus:9090](http://prometheus:9090)). Odată adăugat Prometheus ca sursă,
se pot crea dashboard-uri pe metricile expuse de Prometheus, așa cum se poate observa
în imaginea de mai jos.

<img alt="img" src="/img/softwareDevelopment/lab5_grafana.png" width="900" style={{margin: "auto", display: "block"}} />