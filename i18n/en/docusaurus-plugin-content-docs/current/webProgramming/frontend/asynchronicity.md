---
title: Asincronicitate in React
sidebar_position: 6
---

## Ce este asincronitatea și de ce este importantă în React?

Asincronicitatea este capacitatea unei aplicații de a rula multiple computații în paralel, fără să fie blocată de procesările care durează mai mult sau care necesită așteptare. În React, asincronicitatea este importantă pentru că aplicațiile sunt construite în mare parte în jurul evenimentelor și a datelor care se schimbă în timp real. Prin urmare, este important să putem rula procese asincrone în timp ce aplicația rămâne responsivă.

De exemplu, atunci când facem o cerere către un server pentru a obține informații pentru a afișa pe o pagină, aceasta poate dura ceva timp. În acest timp, aplicația nu trebuie să fie blocată sau să înceteze să funcționeze. În schimb, React poate utiliza asincronicitatea pentru a rula procesul în fundal, fără să blocheze restul aplicației.

În plus, asincronicitatea este importantă în React pentru a gestiona evenimente și animații în timp real. De exemplu, atunci când utilizatorul face clic pe un buton pentru a efectua o acțiune, aplicația trebuie să răspundă rapid pentru a da utilizatorului un raspuns în timp real. Prin utilizarea asincronicității, aplicația poate rula procesare în fundal fără a bloca restul aplicației și fără a încetini răspunsul.

În concluzie, asincronicitatea este importantă pentru a asigura o performanță bună și o experiență de utilizare plăcută. Prin rularea proceselor asincrone, aplicația poate rămâne responsivă și poate gestiona evenimente și date în timp real.

## Utilizarea de API-uri asincrone în React

În React, putem utiliza API-uri asincrone pentru a obține și a procesa date de la servere externe. Acest lucru poate fi făcut folosind funcții precum **fetch**, **axios** sau **XMLHttpRequest** pentru a efectua cereri HTTP către un server. API-urile asincrone sunt esențiale în dezvoltarea aplicațiilor moderne, deoarece permit obținerea de date în timp real fără a fi nevoie de încărcarea unei întregi pagini.

Pentru a utiliza API-uri asincrone în React.js, trebuie să creăm o componentă care să utilizeze o astfel de funcție. O abordare comună este de a utiliza **fetch** pentru a efectua o cerere GET către server pentru a obține datele. În momentul în care datele sunt primite, putem folosi metoda **setState** pentru a actualiza starea componentei cu noile date.

```tsx showLineNumbers
import React, { useEffect, useState } from 'react';

type GetDataState = {
  data: { id: number, title: string }[] | null,
  isLoading: boolean,
  error: { message: string } | null
}

export const TaskList = () => {
  const [state, setState] = useState<GetDataState>({
    data: null, // setam starea inițiala unde datele nu sunt încă disponibile
    isLoading: false, // vrem să știm și cănd datele se încarcă
    error: null // dacă avem erori putem face ceva cu ele, nu să le ignorăm
  });

  useEffect(() => { // folosim useEffect ca la inițializarea/actualizarea componentei să efectuam acțiuni suplimentare
    if (state.data !== null || state.isLoading) { // daca datele sunt încarcate sau pe cale să fie încărcate nu facem nimic
      return;
    }

    setState({ isLoading: true, data: null, error: null }); // setam aici că datele se încarcă

    fetch('https://jsonplaceholder.typicode.com/todos') // efectual operația asincronă
      .then(response => response.json()) // când datele sunt disponibile le deserializăm
      .then(data => setState({ data, isLoading: false, error: null })) // cu obiectul deserializat setăm noua stare
      .catch(error => setState({ error, isLoading: false, data: null })); // dacă a apărut o eroare o setăm în stare
  }, [state]); // lista ca parametru este folosită pentru a apela funcția din useEffect atunci cand se schimbă obiectele din listă

  // în funcție de starea curentă afisăm corespunzator compoenta
  if (state.error) {
    return <p>{state.error.message}</p>;
  }

  if (state.isLoading) {
    return <p>Loading...</p>;
  }

  return <div>
    <h1>Task list:</h1>
    <ul>
      {state.data?.map(task => <li key={task.id}>{task.title}</li>)} 
    </ul>
  </div>
}
```

:::note
În acest exemplu, am utilizat **fetch** pentru a obține o listă de sarcini de pe serverul JSONPlaceholder. Am actualizat starea componentei cu datele obținute din server utilizând metoda **setState**. Dacă întâmpinăm o eroare în timpul procesului de obținere a datelor, afișăm un mesaj de eroare. În cele din urmă, am afișat lista de sarcini printr-o mapare a datelor primite într-o listă. Aici am mai folosit și functia hook **useEffect** care apelează automat funcția dată ca parametru la inițializarea sau actualizarea componentei atunci cand lista de dependente dată s-a modificat, în acest caz starea expusă prin **useState**.
:::

În concluzie, utilizarea API-urilor asincrone în React este esențială pentru a obține date din servere externe și pentru a actualiza componentele în timp real fără a fi nevoie de încărcarea unei întregi pagini.

:::warning
Acest exemplu este folosit doar ca demonstrație pentru folosirea funcțiilor asincrone in React si cum se poate lucra cu acestea în mod general. Totuși, în practica se folosesc diverse pachete și unelte care să faciliteze și să abstractizeze lucrul cu cereri asincrone. Din acest motiv, urmăriti documentația nostră pentru folosirea bibliotecii **ReactQuery** și a utilitarului **OpenAPI Generator**.
:::