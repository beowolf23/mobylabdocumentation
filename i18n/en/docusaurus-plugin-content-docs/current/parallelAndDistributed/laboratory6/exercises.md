---
title: Exercises
sidebar_position: 7
---

1. Starting from the [lab skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab06), solve the synchronization problem in the *multipleProducersMultipleConsumers* package using ArrayBlockingQueue.
2. Solve the synchronization problem in the *synchronizationProblem* package using an AtomicInteger.
3. Solve the synchronization problem in the *bugConcurrentHashMap* package using methods from the ConcurrentHashMap class.
4. Parallelize the program in the *synchronizedSortedList* package, where:
   * Three threads read numbers from three files (one thread reads one file each) and add the numbers from the files to a shared list.
   * The fourth thread sorts the list of numbers.
   * The list can be sorted **only** after all three threads have completed reading from the files.
   * Synchronization between threads must be achieved using a semaphore.
5. Parallelize the program in the *parallelTree* package, where:
   * Two threads build a binary tree in parallel by reading the IDs and the parent of the new nodes from two files and then performing the insert operation.
   * The third thread checks if the tree has been constructed correctly.
   * Checking the tree can only be done after its construction is complete.
   * Synchronization between threads must be achieved using a barrier.
   * During tree insertion, synchronization should not be global (across the entire tree) but should be performed for each node in the tree.
