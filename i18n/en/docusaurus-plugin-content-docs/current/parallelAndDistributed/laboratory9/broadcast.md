---
title: Broadcast
sidebar_position: 3
---

The broadcast operation is an operation in which one process sends a value to all processes within the communicator. In MPI, this operation is represented by MPI_Bcast (Laboratory 8).

Here are attached slides that detail the implementation steps of the broadcast operation: [slides](/files/parallel-and-distributed/broadcast.pdf)

Pseudocode:

```c showLineNumbers
for (pas = 1; pas < nr_procese; pas *= 2)
	if (rank < pas and rank + pas < nr_procese)
		trimite la procesul cu rank-ul [rank + pas]
        else if (rank >= pas and rank < pas * 2)
                primește de la procesul cu rank-ul [rank - pas]
```