---
title: Exercises
sidebar_position: 5
---

1. Starting from the [code skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab05/src), in the **oneProducerOneConsumer** package, implement the Producer-Consumer algorithm for a buffer of size 1.
2. Modify the Producer-Consumer algorithm to accept multiple producers and multiple consumers. Also, modify the buffer to have a size greater than 1. Start from the skeleton in the **multipleProducersMultipleConsumersNBuffer** package.
3. Solve the dining philosophers problem (in the **philosophersProblem** package) and explain it.
4. Solve the Readers-Writers problem using the first conditional synchronization solution in the course (in the **readersWriters.conditionedSynchronization** package).
5. Solve the Readers-Writers problem where writers have priority (in the **readersWriters.writerPriority** package).
6. Solve the barber problem (in the **barber** package).

:::caution
The exercises in this laboratory must be solved using synchronization mechanisms such as wait/notify/notifyAll primitives or semaphores. Solutions that use concurrent objects will not be accepted.
:::
