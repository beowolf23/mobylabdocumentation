---
title: Atomic Variables
sidebar_position: 5
---

In laboratory session 2, we observed that the increment operation (as demonstrated with the statement a+=2) is not an atomic operation (an operation that cannot be divided when executed by a thread). Java provides support for various data types (atomic types) that come with associated atomic operations (more details [here](https://docs.oracle.com/javase/8/docs/api/?java/util/concurrent/atomic/package-summary.html) and [here](https://docs.oracle.com/javase/tutorial/essential/concurrency/atomicvars.html)). These are useful for use as counters or accumulators without the need for synchronization mechanisms.
