---
title: Exercises
sidebar_position: 6
---

1. Create a program that launches a number of threads equal to the number of cores available on your computer. Each thread should print a text like "Hello from thread #id" to the console.

2. Fix the bugs present in the [laboratory skeleton](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab04). Use the **hints** from the sources.

3. Parallelize the doubling of elements in an array, starting from the laboratory skeleton, where you have the serial version.

4. Parallelize the Floyd-Warshall algorithm, starting from the laboratory skeleton, where you have the serial version.
