---
title: Broadcast
sidebar_position: 3
---

## MPI_Bcast

MPI_Bcast is a function through which a process sends a message to all processes in the communicator (message broadcast), including itself.

:::caution
Within the implementation of MPI_Bcast, actions for sending and receiving messages are performed, so you should not call MPI_Recv.
:::

The function signature is as follows:

```c showLineNumbers
int MPI_Bcast(void* data, int count, MPI_Datatype datatype, int root, MPI_Comm communicator)
```
Where:

* **data** (↓ + ↑) - represents the data that is transmitted to all processes. This parameter is of type input for the process with the root identifier and is of type output for the other processes.
* **count** (↓) - the size of the data being sent
* **datatype** (↓) - the data type of the data being sent
* **root** (↓) - the rank / identifier of the source process that sends data to all processes in the communicator, including itself
* **tag** (↓) - message identifier
* **communicator** (↓) - the communicator within which data is sent to all processes within it (usually **MPI_COMM_WORLD**)

An illustration depicting how MPI_Bcast works is shown below:

<img alt="img" src="/img/parallel-and-distributed/mpiBcast.png" width="50%" style={{margin: "auto", display: "block"}} />