---
title: Exercises
sidebar_position: 5
---

The laboratory skeleton is available [here](https://github.com/APD-UPB/APD/tree/master/laboratoare/lab08).

* **Exercise 1**: Implement the ring algorithm using MPI, where the process with ID 0 sends a random number to process 1 (its neighbor), and then the other nodes receive the number from the previous process, increment it by 2, and send it to the next process (for example, process 2 receives the value from process 1, increments it, and sends it further to process 3), all ending when the value reaches process 0. For each process, you should display its rank and the received value.
* **Exercise 2**: Implement an MPI program where process 0 sends a randomly generated value to the other processes using **MPI_Bcast**. **Note**: Do not use **MPI_Recv**.
* **Exercise 3**: Implement an MPI program with N processes, where process 0 initializes an array of size 5 * N with 0, divides it (the chunk size is 5, defined in the skeleton), and sends it to all processes using **MPI_Scatter**. The processes will add each element received with their rank, and then they will send the arrays they operated on, along with their ranks, to process 0 using **MPI_Gather**. Process 0 will subsequently display the result array from **MPI_Gather**.
* **Exercise 4**: Implement an MPI program with 4 processes, where 3 processes send a value to the fourth process, which will receive the value using **MPI_ANY_SOURCE** and display the received value and the source process's rank using **MPI_SOURCE** from **MPI_Status** (status from **MPI_Recv**).

:::tip
**MPI_ANY_SOURCE** is used instead of the source process's rank in **MPI_Recv**, indicating that the receiving process can receive a message from any process.
:::

* **Exercise 5**: Implement an MPI program with 2 processes, where process 0 sends 10 values with different tags to process 1, and process 1 receives the values using the **MPI_ANY_TAG** parameter in **MPI_Recv**. Display the received value along with its tag using **MPI_Status** (status from **MPI_Recv**).

:::tip
**MPI_ANY_TAG** is used instead of the message tag in **MPI_Recv**, indicating that it can receive any message from the source process, regardless of the tag set by the source process in **MPI_Send**.
:::

* **Exercise 6**: Start a number of processes N divisible by 4. Divide the processes into groups of 4 and have them send their ranks in a circle (ring). Display the old rank along with the size of the old group. Display the new rank along with the size of the new group. Each process is allowed to communicate only within its group. Use **MPI_Comm_split**. Each process will display its rank in the new group, the group number, and the number it received.