---
title: C# Workshop
sidebar_position: 1
---

# Fundamentals of OOP in C#

<img alt="C#" src="/img/dotnet/csharp.webp" width="200" style={{float: "right"}} />

**Description**: The summer school aims to introduce the concepts of Object-Oriented Programming (OOP) using examples in C#, as well as the development of applications using UI frameworks like WPF or MAUI.

**Target audience**: 1st and 2nd year students

**Format**: In-person, 10-12 sessions of ~2 hours, spaced 2 days apart

**Maximum number of participants**: 20

**Duration**: Starting from July 13th

**Presenter**: Silviu-George Pantelimon

# Concepts covered in this workshop

1. Introduction to .Net architecture and programming paradigms in general
2. Data types: value types and reference types, primitives and objects
3. In-depth look at classes, access modifiers, concepts of static vs. non-static
4. Inheritance and polymorphism, abstract classes and interfaces
5. Generics
6. Collections, enumerations, and other useful classes
7. Inheritance vs. partial classes vs. extension methods
8. Exceptions and error handling
9. Threads and tasks
10. IO operations, working with streams
11. Reflection and dependency injection
12. Applications in .NET

To better understand the concepts presented here, you can access the project with examples on Gitlab at this address: [https://gitlab.com/mobylabwebprogramming/dotnetworkshop](https://gitlab.com/mobylabwebprogramming/dotnetworkshop).