---
title: Development Environment and Code Guidelines
sidebar_position: 3
---

To start programming in C#, you need an **IDE** and packages for **.NET 6** (the necessary dependencies will be installed automatically):

1. For Windows, we recommend **[Visual Studio 2022 Community](https://visualstudio.microsoft.com/vs/)**, or you can access the **[Professional or Enterprise](https://azureforeducation.microsoft.com/devtools)** version through **[MyUPB](https://my.upb.ro/)**
2. For Linux/MacOSX or if you prefer an alternative, you can use the **[DotUltimate](https://www.jetbrains.com/dotnet/)** platform with **[Rider](https://www.jetbrains.com/rider/)** as the IDE. You can request an academic license for the entire JetBrains suite using your institutional email. You may need to separately install the SDK for **[.NET 6](https://dotnet.microsoft.com/en-us/download/visual-studio-sdks)**.
3. **Bonus**: You can use **Visual Studio** with **ReSharper** from **JetBrains** for a better IDE experience, but you'll need at least 16GB of RAM.

## Code Guidelines

- Avoid declaring variables at the beginning of a function; declare them just before use with initialization. The greater the distance between variable declaration and usage, the more likely you are to introduce errors.
- Use camel-case or pascal-case as needed, and never use snake-case. You can refer to C# naming conventions [here](https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/coding-style/identifier-names).
- Blocks inside if/for/while should always be enclosed in braces, even if there's only a single line of code. Incorrect indentation can lead to errors.
- Avoid using static/global variables, only constants should be static and global. Static/global variables can cause problems, especially with concurrent access.
- Use a version control service like **GitHub/GitLab/Bitbucket**, and make multiple commits with descriptive messages to avoid losing your work if mistakes occur.
- Avoid creating excessively long functions; it's easier to analyze code in smaller chunks.
- Prefer generating new values over passing information through side effects. This can help avoid errors since the program's flow is well-defined.
- Don't create utility functions for simple tasks; these functions likely already exist in the standard library, such as "string.IsEmptyOrWhiteSpace" to check if a string is null, empty, or whitespace.
- Opt for using libraries from **[NuGet](https://www.nuget.org/)** instead of building your own code infrastructure from scratch. Use well-maintained libraries with many downloads that are compatible with your framework version.
- Use the debugger in your **IDE** instead of printing variables; it's easier to find errors through the interface where you can view variables, evaluate expressions, and navigate through stack frames.
- Consult the official C# documentation from **[Microsoft](https://learn.microsoft.com/en-us/dotnet/csharp/)**, as well as the documentation for libraries downloaded from **NuGet**.

:::tip Tip
It's important to emphasize the need to use an IDE. An IDE not only streamlines your work and project management but also educates you through its suggestions on how to follow code and identify potential issues using static code analysis. Additionally, integrated debugging tools and other features can greatly enhance your development and debugging efficiency.
:::

## Minimum Program Structure

Here's the minimum structure of a C# program. Note that there are no functions outside of classes, and classes are organized in namespaces.

```csharp showLineNumbers
using System; // Using the using keyword, classes/symbols from the required namespace can be imported.

namespace MyNewProject; // Declare the current namespace that contains classes declared in the file.

public class Program // There must always be a class for the main function.
{
    /* Declare the Main static function as the entry point of the program.
     * Parameters can be omitted and represent command-line arguments (not including the executable name as in C).
     */
	public static void Main(string[] args) {
	    // ...
    }
}
```

A **namespace** is a logical grouping of classes/structures in a program. It functions similarly to namespaces in C++ and is equivalent to packages in Java. There is often confusion between a **namespace** and an **assembly**. An assembly is the compiled code intended to be utilized by the .NET runtime as either a dynamically linked library (DLL) or an executable (EXE). An assembly can encompass multiple namespaces, and a namespace can be found in multiple assemblies.

## Exercises

1. Install Visual Studio/Rider.
2. Create a console app project in Visual Studio/Rider and attempt to write a minimal program.
3. Establish a Git repository on [Gitlab](https://gitlab.com/) and commit your project from Visual Studio/Rider using the IDE's git features.
