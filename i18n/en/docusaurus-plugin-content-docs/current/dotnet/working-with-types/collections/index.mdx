---
title: Collections
sidebar_position: 6
---

# Collections

In all object-oriented (and functional) languages, there are many data structures designed to assist programmers for various applications. A significant group of useful data structures is known as collections. A collection is a generic class that organizes multiple objects in a specific manner and provides operations for access and modification. The usefulness of collections lies in their specialization, which determines how much memory and time are consumed for different operations.

The most commonly used collections are lists, sets, and dictionaries/maps:
* **List&lt;T&gt;** - the most commonly used type of list in C#, with the underlying implementation being an array list. It has good performance for indexed access in **O(1)**, but insertions and deletions are in the worst case **O(n)**.
* **LinkedList&lt;T&gt;** - implements a doubly linked list with slightly poorer performance compared to **List&lt;T&gt;**. Indexed access is **O(n)**, but insertion and deletion at the list ends are **O(1)**.
* **HashSet&lt;T&gt;** - an unordered set with high performance that does not allow duplicate elements. It uses the **GetHashCode()** and **Equals()** methods of type T to determine where an element should be placed in its internal structure, which is a hash table. This means T needs to override these methods. It has near **O(1)** average performance for all operations on average types but can become **O(n)** for a large number of elements.
* **SortedSet&lt;T&gt;** - an ordered set with constant good performance as it maintains element order and doesn't allow duplicates. Elements of type T must implement **IComparable&lt;T&gt;** or be given an **IComparator&lt;T&gt;** in the collection's constructor to establish the element order. It internally maintains a red-black tree. Access and modification operations on this set are always **O(log n)**.
* **Dictionary&lt;TKey, TValue&gt;** - an unordered map/table with keys of type TKey to access values of type TValue. It internally maintains a hash table like **HashSet** and behaves similarly, relying on TKey's **GetHashCode()** and **Equals()**.
* **SortedDictionary&lt;TKey, TValue&gt;** - an ordered map/table with keys of type TKey to access values of type TValue. It internally maintains a red-black tree like **SortedSet** and behaves similarly, relying on an **IComparator&lt;TKey&gt;** or TKey implementing **IComparable&lt;TKey&gt;**.
* **SortedList&lt;TKey, TValue&gt;** - works similarly to **SortedDictionary&lt;TKey, TValue&gt;** but with an internal sorted list. Due to this, although access is **O(log n)**, insertions and deletions are **O(n)**, making it faster only for indexed value access.

Other useful collections include:
* **Stack&lt;T&gt;** - implements a Last-In-First-Out (LIFO) queue.
* **Queue&lt;T&gt;** - implements a First-In-First-Out (FIFO) queue.
* **PriorityQueue&lt;TElement, TPriority&gt;** - implements a priority queue for elements of type TElement, ordered by values of type TPriority, given an **IComparator&lt;TPriority&gt;** or having TPriority implement **IComparable&lt;TPriority&gt;**.

As you can see, you need to be cautious about which data structure you choose for your programs because it will impact performance and the logic of your program's use of one collection over another. Most likely, you'll use **List&lt;T&gt;** and **Dictionary&lt;TKey, TValue&gt;** classes in most cases, but it's good to know about the other data structures when you need them.

## Resources

* [System.Collections.Generic](https://learn.microsoft.com/en-us/dotnet/api/system.collections.generic?view=net-6.0)

import DocCardList from '@theme/DocCardList';

<DocCardList />