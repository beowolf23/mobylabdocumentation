FROM node:18-alpine AS build

WORKDIR /app
COPY package*.json ./

RUN npm ci
ENV PATH /app/node_modules/.bin:$PATH
RUN npm install react-scripts -g
COPY . .

RUN NODE_OPTIONS="--max-old-space-size=4096" npm run build

FROM nginx:1.25.4
COPY --from=build /app/build /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]